function masterscript(conditionNum, mode, subjectNum, role, ip, port)
% This is a masterscript for the Mouse-tracking Social Simon task experiment.
% It initializes the experiment and saves the data.
%
% conditionNum (int): condition ID, 1=individual, 2=social_invisible,
%                       3=social_visible
% subjectNum (int): the participant number used for data storage;
%                   in the social conditions, the number refers to the 
%                   pair of subjects

addpath(genpath('/Applications/Psychtoolbox/'));
%cd('/Users/katja/matlabwd/twomice')

% set data folder:
data_path = [pwd '/Data/'];
addpath(data_path);

%----------------------------------------------------------------------
%                     Psychtoolbox housekeeping
%----------------------------------------------------------------------
% get old Psychtoolbox preferences
oldVisualDebugLevel = Screen('Preference', 'VisualDebugLevel');
oldSupressAllWarnings = Screen('Preference', 'SuppressAllWarnings');
oldSyncTests = Screen('Preference', 'SkipSyncTests');

% set new preferences
Screen('Preference', 'VisualDebugLevel', 3);
Screen('Preference', 'SuppressAllWarnings', 1);
Screen('Preference', 'SkipSyncTests', 1);

% initialize Psychtoolbox
ptb = initPTB_simon();

if conditionNum == 1
    practiceorderFileDemo = strcat(data_path, 'practiceorderExp1Demo.mat');
    trialorderFileDemo = strcat(data_path, 'trialorderExp1Demo.mat');
    practiceorderFile = strcat(data_path, 'practiceorderExp1.mat');
    trialorderFile = strcat(data_path, 'trialorderExp1.mat');

    practiceTrialMatrixDemo = importdata(practiceorderFileDemo);
    trialMatrixDemo = importdata(trialorderFileDemo);

    practiceTrialMatrix = importdata(practiceorderFile);
    trialMatrix = importdata(trialorderFile);

    cond_path = strcat(data_path, 'Experiment1/');
    % Display instructions
    displayInstructions(conditionNum, ptb);
    KbWait([],1);
    if strcmp(mode, 'demo')
        % Run practice rounds
        runPractice1(practiceTrialMatrixDemo{1,1}, ptb);
        interInfo = (['You have completed the practice rounds.\n\n',...
            'Press space to begin the experiment']);
        displayMsg(interInfo, ptb);
        % Run experiment
        respMat = exp1(trialMatrixDemo{subjectNum}, ptb);
        % Save data
        responsesFile = sprintf('DemoResponses_%i.mat', subjectNum);
        save(strcat(cond_path, responsesFile), 'respMat');
    elseif strcmp(mode, 'test')
        % Run practice rounds
        runPractice1(practiceTrialMatrix{1,1}, ptb);
        interInfo = (['You have completed the practice rounds.\n\n',...
            'Press space to begin the experiment']);
        displayMsg(interInfo, ptb);
        % Run experiment
        respMat = exp1(trialMatrix{subjectNum}, ptb);
        % Save data
        responsesFile = sprintf('Responses_%i.mat', subjectNum);
        save(strcat(cond_path, responsesFile), 'respMat');
    end

elseif conditionNum == 2
    cond_path = strcat(data_path, 'Experiment2/');
    % Display instructions
    displayInstructions(conditionNum, ptb)
    % Run practice rounds
    runPractice2(practiceTrialMatrix{1,1}, ptb);
    interInfo = (['You have completed the practice rounds.\n\n',...
        'Press space to begin the experiment']);
    displayMsg(interInfo, ptb);
    % Run experiment
    respMat = exp2(trialMatrix{subjectNum}, ptb, role, ip, port);
    % Save data
    responsesFile = sprintf('Responses_%i.mat', subjectNum);
    save(strcat(cond_path, responsesFile), 'respMat');

elseif conditionNum == 3
    cond_path = strcat(data_path, 'Experiment3/');
    % Display instructions
    displayInstructions(conditionNum, ptb)
    % Run practice rounds
    runPractice2(practiceTrialMatrix{1,1}, ptb);
    interInfo = (['You have completed the practice rounds.\n\n',...
        'Press space to begin the experiment']);
    displayMsg(interInfo, ptb);
    % Run experiment
    respMat = exp3(trialMatrix{subjectNum}, ptb);
    % Save data
    responsesFile = sprintf('Responses_%i.mat', subjectNum);
    save(strcat(cond_path, responsesFile), 'respMat');

else
    disp('Unknown condition');
end

endInfo = 'Experiment finished. \n\n Press space to exit.';
displayMsg(endInfo, ptb);

cleanup();
% [~, keyCode] = KbWait;
% if keyCode(ptb.space)
%     cleanup();
% end

function cleanup()
    try closePTB(); catch, end
    try psychrethrow(psychlasterror); catch, end
end

%----------------------------------------------------------------------
%                     Psychtoolbox housekeeping
%----------------------------------------------------------------------
% Restore preferences
Screen('Preference', 'VisualDebugLevel', oldVisualDebugLevel);
Screen('Preference', 'SuppressAllWarnings', oldSupressAllWarnings);
Screen('Preference', 'SkipSyncTests', oldSyncTests);

end