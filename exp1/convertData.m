function convertData

data_path = [pwd '/Data/Experiment1'];
addpath(data_path);

results_path = [pwd '/Results/'];

for i=1:20
    data2csv(i);
end


function data2csv(subjNum)

resultsFile = sprintf('Responses_%i.mat', subjNum);
respMat = importdata(resultsFile);

[~, ncols] = size(respMat);

for col = 1:ncols
    respMat(col).x = respMat(col).coordinates(:,1);
    respMat(col).y = respMat(col).coordinates(:,2);
    respMat(col).trial = col;
    respMat(col).subid = sprintf('%03d', subjNum);
end

respMat2 = rmfield(respMat, 'coordinates');
temp_table = struct2table(respMat2);

temp_table = temp_table(:,[1:3 end-1:end 4:end-2]);

csvname = sprintf(strcat(results_path, 's%s.csv'), respMat2(1).subid);
writetable(temp_table, csvname);

end

end