function displayInstructions(conditionNum, ptb)

% A script to display experimental instructions
    if conditionNum == 1
        % individual
        startInfo1 = fileread('simon_instructions1.txt');
        startInfo2 = fileread('simon_instructions2.txt');
    elseif conditionNum == 2 || 3
        % social
        startInfo1 = fileread('soc_simon_instructions1.txt');
        startInfo2 = fileread('soc_simon_instructions2.txt');
    end

    
    textpage = 1;

    while 1
      if textpage == 1
        % Present a start screen and wait for a key-press
        DrawFormattedText(ptb.window, startInfo1, 150, 50, ptb.white, [], [], [], 1.25);
        Screen('Flip', ptb.window);
      
        [~, keyCode] = KbWait;
        if keyCode(ptb.rightarrow)
          textpage = 2;
        end
      end

      if textpage == 2
        % Present a start screen and wait for a key-press
        DrawFormattedText(ptb.window, startInfo2, 150, 50, ptb.white, [], [], [], 1.25);
        Screen('Flip', ptb.window);
        
        [~, keyCode] = KbWait;
        if keyCode(ptb.leftarrow)
          textpage = 1;
        elseif keyCode(ptb.space)
          break;
        end
      end
    
    end

end