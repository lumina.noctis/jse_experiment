function plotTrajectories(respMat, trialNum, mode, height, width, fromSample, toSample)
  % given a particular response matrix respMat
  % and a trial number trialNum
  % make plots of mouse trajectories for single participant ('single' mode)
  % or two participants in the same figure ('double' mode)
  % screen pixels 1920 * 1080

  numSamples = length(respMat(trialNum).coordinates);

  if nargin < 6
    fromSample = 1;
    toSample = numSamples;
  end
  
  if toSample > numSamples
    error('Your toSample argument has to be at most %d', numSamples);
  end

  if strcmp(mode,'single')
    points = respMat(trialNum).coordinates;
    % Plot the contour in a Matlab figure
    if respMat(trialNum).cueColor == 1
        if respMat(trialNum).cuePos == 1
            plot(points(fromSample:toSample,1)-width/2, height-points(:,2), '-b');
        else
            plot(points(fromSample:toSample,1)-width/2, height-points(:,2), ':b');
        %plot(points(fromSample:toSample,1), height-points(:,2), 'b');
        end
    else
        if respMat(trialNum).cuePos == 2
            plot(width-points(fromSample:toSample,1)-width/2, height-points(:,2), '-r');
        else
            plot(width-points(fromSample:toSample,1)-width/2, height-points(:,2), ':r');
        %plot(points(fromSample:toSample,1), height-points(:,2), 'r');
        end
    end
    xlim([-width/2,width/2]);
    %xlim([0,width]);
    ylim([0,height]);
  elseif strcmp(mode,'double')
    pointsA = respMat{5, trialNum};
    pointsB = respMat{6, trialNum};
    % Plot the contour in a Matlab figure
    hold off;
    figure(1)
    plot(pointsA(fromSample:toSample,1),900-pointsA(fromSample:toSample,2),'r');
    hold on;
    plot(pointsB(fromSample:toSample,1),900-pointsB(fromSample:toSample,2),'b');
    xlim([0,1600]);
    ylim([0,900]);
  end
  
  
% plotTrajectories(respMat, 1, 'single')
% hold on
% for i=2:640
% plotTrajectories(respMat, i, 'single')
%end
end