function displayMsg(message, ptb)

    while 1
        % Present a message and wait for a key-press
        Screen('FillRect', ptb.window, ptb.black);
        DrawFormattedText(ptb.window, message, 'center', 'center', ptb.white, [], [], [], 1.25);
        Screen('Flip', ptb.window);

        [~, keyCode] = KbWait;
        if keyCode(ptb.space)
          break;
        end
    end

end