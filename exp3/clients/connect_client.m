%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The BrainStream software is free but copyrighted software, distributed  %
% under the terms of the GNU General Public Licence as published by       %
% the Free Software Foundation (either version 2, or at your option       %
% any later version). See the file COPYING.txt in the main BrainStream    %
% folder for more details.                                                %
%                                                                         %
% Copyright (C) 2009, Philip van den Broek                                %
% Donders Institute for Brain, Cognition and Behaviour                    %
% Radboud University Nijmegen, The Netherlands                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sock = connect_client(dns,port)
tic;
n=0;
twait = 0;
while 1
	sock = msconnect(dns,port);
	if isa(sock,'sun.nio.ch.SocketChannelImpl')
		break % connected
	end
	waitTime = toc;
	if waitTime > Inf %10*60 % waited for more than 10 minutes?
		error(['connection to server failed (elapsed time: ' num2str(waitTime) ' )']);
	end
	twait = min(1,twait + 0.05); % increasing wait times up to maximum of 1 sec.
	pause(twait);
	if ismac % windows mex files for msconnect still print one line output
		n = n + 1;
		fprintf('.');
		if ~mod(n,80), fprintf('\n'); end
		if ~mod(n,800)
			fprintf('Waiting for BrainStream to connect...\n');
		end
	end
end
if isa(sock,'sun.nio.ch.SocketChannelImpl')
	client_socks(sock,'add');
end
