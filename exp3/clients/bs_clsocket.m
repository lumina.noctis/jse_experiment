%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The BrainStream software is free but copyrighted software, distributed  %
% under the terms of the GNU General Public Licence as published by       %
% the Free Software Foundation (either version 2, or at your option       %
% any later version). See the file COPYING.txt in the main BrainStream    %
% folder for more details.                                                %
%                                                                         %
% Copyright (C) 2011, Philip van den Broek                                %
% Donders Institute for Brain, Cognition and Behaviour                    %
% Radboud University Nijmegen, The Netherlands                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function bs_clsocket(sock)
% use this function at client side if the buffer has been filled by
% BrainStream user application and causes BS to not respond anymore until
% all content is read from the buffer

if ~isa(sock,'sun.nio.ch.SocketChannelImpl')
	%disp('close socket: handle to socket not available');
	return
end
if ~sock.isOpen
	return
end
js = jsocket;
js.flush(sock,0);

% success = 1;
% while success >= 0
% 	[vars, success] = ms_recv(js,sock,0);
% 	if success>0
% 		disp('flushed user socket');
% 	end
% end
