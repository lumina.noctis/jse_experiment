%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The BrainStream software is free but copyrighted software, distributed  %
% under the terms of the GNU General Public Licence as published by       %
% the Free Software Foundation (either version 2, or at your option       %
% any later version). See the file COPYING.txt in the main BrainStream    %
% folder for more details.                                                %
%                                                                         %
% Copyright (C) 2009, Philip van den Broek                                %
% Donders Institute for Brain, Cognition and Behaviour                    %
% Radboud University Nijmegen, The Netherlands                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% maintains list of sockets
function ret = client_socks(sock,mod)
mlock
persistent socks
if nargin == 1 && ischar(sock) 
	if ~any(strcmp(sock,{'get','clear','closeall'}))
		error('Arguments don''t match, specify socket identifier and modifier');
	end
	mod = sock; 
end

ret = [];
switch mod
	case 'get'
		ret = socks;
	case 'add'
		socks{end+1} = sock;
	case 'remove'
		% remove sock from the list
%		socks(socks==sock) = [];		
		socks(cellfun(@isequal,socks,repmat({sock},1,numel(socks)))) = [];
	case 'clear'
		socks = [];
	case 'closeall'		
		if ~isempty(socks)
			% close possible remaining socket after failure in previous session
			for n = 1 : numel(socks)
				bs_clsocket(socks{n}); % flush
				msclose(socks{n});
			end
			socks = [];
		end		
	otherwise
		error('Unknown modifier specified');
end