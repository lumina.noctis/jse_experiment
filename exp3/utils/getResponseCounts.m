function correctProps = getResponseCounts(nSub)

data_path = [pwd '/Data/Experiment2'];
addpath(data_path);

correctProps = zeros(nSub, 2);

for i=1:nSub
    correctProps(i, 1) = i;
    correctProps(i, 2) = countResponses(i);
end



function correctProp = countResponses(subjNum)
    resultsFile = sprintf('Responses_%i.mat', subjNum);
    respMat = importdata(resultsFile);

    [~, ncols] = size(respMat);
    
    numCorrect = 0;
    b = respMat(1).block;

    for col = 1:ncols
        c = respMat(col).cueColor;
        r = respMat(col).selectedBox;
        if b == 1 || b == 3
            if c == 1
                if c == r
                    numCorrect = numCorrect + 1;
                end
            else
                if r < 0
                    numCorrect = numCorrect + 1;
                end
            end
        elseif b == 2 || b == 4
            if c == 2                         
                if c == r
                    numCorrect = numCorrect + 1;
                end
            else
                if r < 0
                    numCorrect = numCorrect + 1;
                end
            end
        end
    end
    
    correctProp = numCorrect/ncols;
end

end




                