%install_twomice();
%twomice_exchange_mousepositions('client','localhost',1972)
%twomice_exchange_mousepositions('server','localhost',1972)
function twomice_test_connection(role,ip,port)
persistent buffer

% install java FieldTrip buffer classes
javaaddpath(which('ft_bufferserver.jar'));
javaaddpath(which('BufferClient.jar'));

% import java classes
import nl.fcdonders.fieldtrip.bufferserver.*
import nl.fcdonders.fieldtrip.bufferclient.*

% get object to access static list of running fieldtrip servers
srv = BufferServer;

% make sure to stop possible earlier started buffer
stopBuffer();

% add required folders to Matlab path
thisfilespath = fileparts(mfilename('fullpath'));
addpath(genpath(thisfilespath));

% make sure cleanup procesdure also executes after breaking out with ctrl-c
clean = onCleanup(@()cleanup()); % executes at cleanup of local variable clean

try
   
%data channels: role (server=1,client=2), time, mouse x-position, mouse y-position, trial number, stage number, button1, button2, button?  
% channel indices into mouse data
ROLE=1;
roles={'server','client'};

% Get FieldTrip buffer server object
buffer=BufferServer();
% Get client object that connects to fieldtrip buffer
client=BufferClient;    

tic;
if strcmp(role,'server')
   time = toc;
   rolenumber = 1;
   install_fieldtrip_server();   
elseif strcmp(role,'client')
   time=[]; 
   rolenumber = 2;   
   connect_client();   
else
   error('Role should be either ''server'' or ''client''');
end
fprintf('\nConnected to FieldTrip buffer\n');

nEvents = 0;   % keep track of number of processed fiedltrip events (not used)
nSamples = 0;  % keep track of number of processed samples thusfar
tic
while (1)
   
   % get new remote mouse positions
   receive
   
   if ~isempty(ret)
      % show received info:
      fprintf('Received time from %s: %f\n',roles{ret(1)},ret(end,2));
      if strcmp(role,'client') && isempty(time)
         tic;
         time = ret(end,2);
      end
   end
   % send this computers time   
   data = [rolenumber; time+toc]; 
   client.putData(data(:),[size(data,2) size(data,1)]);       
   
   java.lang.Thread.sleep(1000);
end

%----------------------------------------------------------------------
%                     Display end message
%----------------------------------------------------------------------

cleanup();

catch err
   disp(err.message);
   cleanup();
end

   function receive
      ret = []; % init
      hdr = client.getHeader;
      % check new events (events not used)
      if hdr.nEvents > nEvents
         % get the last event for this process
         evts = client.getEvents(nEvents,hdr.nEvents-1); % only pick last one
         for n = numel(evts) : -1: 1 
            if ~strcmp(char(evts(n).getType),role)
               ret=evts(n).getValue.getArray; % Note: 1-based counting here!!
               break;
            end
         end
         nEvents = hdr.nEvents;
      end
      % check new incoming data
      if hdr.nSamples > nSamples
         % only get the last item
         data = client.getDoubleData(hdr.nSamples-1,hdr.nSamples-1);
         % only interested in remote info, so remove own info
         data(data(:,1)==rolenumber,:) = [];
         if ~isempty(data)
            ret = data;
         end
         nSamples = hdr.nSamples;
      end
   end

   function install_fieldtrip_server()
      % Initiate FieldTrip buffer server
      buffer.BufferServerStart(port,120000,500);
      %buffer.Start(port,120000,500);
      % Connect client to buffer
      client.connect('localhost',port);   
      % Start buffer with correct header, determine number of required channels
      numChannels=2; % role and time
      Fs = 1; % not meaningful (intermittent data, depends on mouse movements)
      dataType = 10; % FLOAT32
      import nl.fcdonders.fieldtrip.bufferclient.Header
      hdr=Header(numChannels,Fs,dataType);
      client.putHeader(hdr);
      fprintf('\nFieldTrip buffer started\n');
   end

   function connect_client()
      client.connect(ip,port); % Connect client to remote fieldtrip server
      fprintf('Waiting for FieldTrip buffer...\n');
      while 1
         try
            client.getHeader()
         catch
             pause(0.1);
             continue
         end
         break % we're connected
      end
   end

   function cleanup()
      stopBuffer();
   end

   function stopBuffer()
      if exist('buffer','var') && ~isempty(buffer)
         try buffer.stopBuffer(); catch, end  
         buffer = [];
      end
      import nl.fcdonders.fieldtrip.bufferserver.*
      srv = BufferServer;
   %   srv.removeAllServerSockets;
   end

%    function send(value,sample)
%       evt = BufferEvent(role,value,sample); % sample always 1 ??
%       client.putEvent(evt);
%    end
end
