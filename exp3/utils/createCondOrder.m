% Initialize folder paths
addpath(genpath('/Applications/Psychtoolbox/'));
cd('/Users/katja/matlabwd/KatjaCCS3')

% Set data folder
data_path = [pwd '/Data/'];
addpath(data_path);

% Set utils folder
utils_path = [pwd '/utils/'];
addpath(utils_path);


% Initialize file names
condOrderFile = strcat(data_path, 'condOrder.mat');

% Seed the random number generator
rng('default');
rng(120);

% 1="pr"; 2="ab"
conds = [1, 2];
numParts = 20;
repeats = numParts/length(conds);
condList = repmat(conds, 1, repeats);
%force the first two runs to be (pr, ab)
condOrder = [condList(1:2), Shuffle(condList(3:end))];

% Save
save(condOrderFile, 'condOrder');


condOrderFile = strcat('/Users/katja/KatjaCCS3/Data/', 'condOrderExtra.mat');
numPartsExtra = 10;
repeatsExtra = numPartsExtra/length(conds);
condListExtra = repmat(conds, 1, repeatsExtra);

rng(132);
condOrder = Shuffle(condListExtra);

% Save
save(condOrderFile, 'condOrder');
