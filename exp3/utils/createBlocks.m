% Initialize folder paths
addpath(genpath('/Applications/Psychtoolbox/'));
cd('/Users/katja/matlabwd/KatjaCCS3')

% Set data folder
data_path = [pwd '/Data/'];
addpath(data_path);

% Set utils folder
utils_path = [pwd '/utils/'];
addpath(utils_path);

% Seed the random number generator
rng('default');
rng(41);

% Initialize file names
blockOrderFile3 = strcat(data_path, 'blockOrderExp3.mat');
blockOrderFile4 = strcat(data_path, 'blockOrderExp4.mat');

blocks = 1:2;
numParts = 20;
repeats = numParts/length(blocks);
blockList = repmat(blocks, 1, repeats);

blockOrder3 = Shuffle(blockList);
blockOrder4 = Shuffle(blockList);

% Save
save(blockOrderFile3, 'blockOrder3');
save(blockOrderFile4, 'blockOrder4');