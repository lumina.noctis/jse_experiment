% Initialize folder paths
addpath(genpath('/Applications/Psychtoolbox/'));
cd('/Users/katja/matlabwd/KatjaCCS3')

% Set data folder
data_path = [pwd '/Data/'];
addpath(data_path);

% Set utils folder
utils_path = [pwd '/utils/'];
addpath(utils_path);

% Seed the random number generator
rng('default');
rng(16);


% Initialize file names
trialorderFileDemo3 = strcat(data_path, 'trialorderExp3Demo.mat');
practiceorderFileDemo3 = strcat(data_path, 'practiceorderExp3Demo.mat');
trialorderFile3 = strcat(data_path, 'trialorderExp3.mat');
practiceorderFile3 = strcat(data_path, 'practiceorderExp3.mat');


trialorderFileDemo4 = strcat(data_path, 'trialorderExp4Demo.mat');
practiceorderFileDemo4 = strcat(data_path, 'practiceorderExp4Demo.mat');
trialorderFile4 = strcat(data_path, 'trialorderExp4.mat');
practiceorderFile4 = strcat(data_path, 'practiceorderExp4.mat');


conditions = [1, 1; 1, 2; 2, 1; 2, 2];

numRepetitionsDemo = 1;
numRepetitions = 10;

numRepetitionsPracticeDemo = 3;
numRepetitionsPractice = 10;

numParticipantsPractice = 1; % the same practice matrix given to all participants
numParticipants = 20;

constraintPractice = 'single trial';
constraintTest = 'two trials';

% Generate trial matrices
practiceMatrixDemo3 = randomizeTrials(conditions, numRepetitionsPracticeDemo, numParticipantsPractice, constraintPractice);
trialMatrixDemo3 = randomizeTrials(conditions, numRepetitionsDemo, numParticipants, constraintTest);
practiceMatrix3 = randomizeTrials(conditions, numRepetitionsPractice, numParticipantsPractice, constraintPractice);
% Create two separate blocks
trialMatrix1 = randomizeTrials(conditions, numRepetitions, numParticipants, constraintTest);
trialMatrix2 = randomizeTrials(conditions, numRepetitions, numParticipants, constraintTest);
% Combine blocks
trialMatrix3 = cell(1, numParticipants);
for i=1:numParticipants
    trialMatrix3{1,i} = [trialMatrix1{1,i}; trialMatrix2{1,i}];
end


% Generate trial matrices
practiceMatrixDemo4 = randomizeTrials(conditions, numRepetitionsPracticeDemo, numParticipantsPractice, constraintPractice);
trialMatrixDemo4 = randomizeTrials(conditions, numRepetitionsDemo, numParticipants, constraintTest);
practiceMatrix4 = randomizeTrials(conditions, numRepetitionsPractice, numParticipantsPractice, constraintPractice);
% Create two separate blocks
trialMatrix1 = randomizeTrials(conditions, numRepetitions, numParticipants, constraintTest);
trialMatrix2 = randomizeTrials(conditions, numRepetitions, numParticipants, constraintTest);
% Combine blocks
trialMatrix4 = cell(1, numParticipants);
for i=1:numParticipants
    trialMatrix4{1,i} = [trialMatrix1{1,i}; trialMatrix2{1,i}];
end


% Save all files
save(trialorderFileDemo3, 'trialMatrixDemo3');
save(practiceorderFileDemo3, 'practiceMatrixDemo3');
save(trialorderFile3, 'trialMatrix3');
save(practiceorderFile3, 'practiceMatrix3');

% Save all files
save(trialorderFileDemo4, 'trialMatrixDemo4');
save(practiceorderFileDemo4, 'practiceMatrixDemo4');
save(trialorderFile4, 'trialMatrix4');
save(practiceorderFile4, 'practiceMatrix4');