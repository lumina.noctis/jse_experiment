function respMat = twomice_simon(role,ip,port)

persistent bufferServer
% install java FieldTrip buffer classes
addjavaclasspath(which('ft_bufferserver.jar'));
addjavaclasspath(which('BufferClient.jar'));

% import java classes
import nl.fcdonders.fieldtrip.bufferserver.*
import nl.fcdonders.fieldtrip.bufferclient.*

% make sure to stop possible earlier started buffer
stop_buffer();

% add required folders to Matlab path
thisfilespath = fileparts(mfilename('fullpath'));
addpath(genpath(thisfilespath));

% make sure cleanup procesdure also executes after breaking out with ctrl-c
clean = onCleanup(@()cleanup()); % executes at cleanup of local variable clean

try
    
    % initialize Psychtoolbox
    ptb = initPTB_simon();
    
    % create texel for extra mousepointer
    % color = 127; % white, 127=grey, 255=white or black???
    % image=ones(20,20,3)*color; % change into cursor image instead of cubic
    % texel = Screen('MakeTexture', ptb.window, image);
    % srcRect = [0;0;size(image,2);size(image,1)];
    
    % Create sprites for both mouse pointers
    imgFile = 'hand1.png';
    [image, ~, alpha] = imread(imgFile);
    image(:,:,4) = alpha(:,:);
    texel = Screen('MakeTexture', ptb.window, image);
    [s1,s2,~] = size(image);
    imgH = s1 * 0.05;
    imgW = s2 * 0.05;
    spriteRect = [0 0 imgW imgH]; % The bounding box for the sprite
    
    HideCursor(ptb.window);
    
    %data channels: role (server=1,client=2), mouse x-position, mouse y-position, trial number, stage number, button1, button2, button?
    % channel index constants into mouse data matrix
    ROLE=1;
    XPOS=2; % x-channel position in mouse data matrix
    YPOS=3; % y-channel position in mouse data matrix
    TRIAL=4;
    STAGE=5;
    roles={'server','client'};
    
    % Get FieldTrip buffer server object
    bufferServer=BufferServer();
    % Get client objects that connect to both fieldtrip buffers
    sndClient=BufferClient; % client for sending mouse data to remote computer
    rcvClient=BufferClient; % client for receiving mouse data from remote computer
    
    if strcmp(role,'server')
        rolenumber = 1;
        svport = port;
        clport = port+1;
    elseif strcmp(role,'client')
        rolenumber = 2;
        svport = port+1;
        clport = port;
    else
        error('Role should be either ''server'' or ''client''');
    end
    
    % Install FieldTrip server
    install_fieldtrip_server(); % install FieldTrip buffer server and connect sndClient to it
    connect_client();   % connect rcvClient to remote computer FieldTrip buffer server
    fprintf('\nConnected to FieldTrip buffer\n');
    
    % Initialize two structs for information exchange between computers
    mousepos = struct('iam',role,'x',0,'y',0,'buttons',[],'trialnum',1,'stagenum',1);
    mousepos_remote = struct('iam',roles{~strcmp(roles,role)},'x',0,'y',0,'buttons',[],'trialnum',1,'stagenum',1);
    
    %----------------------------------------------------------------------
    %                     Make a randomized trial matrix
    %----------------------------------------------------------------------
    
    % Seed the random number generator. Here we use the an older way to be
    % compatible with older systems. Newer syntax would be rng('shuffle'). Look
    % at the help function of rand "help rand" for more information
    rng('default');
    % Make the matrix which will determine our condition combinations
    condMatrixBase = [sort(repmat([1 2], 1, 2)); repmat([1 2], 1, 2)];
    % Number of trials per condition. We set this to one for this demo, to give
    % us a total of 4 trials.
    trialsPerCondition = 1;
    % Duplicate the condition matrix to get the full number of trials
    condMatrix = repmat(condMatrixBase, 1, trialsPerCondition);
    % Get the size of the matrix
    [~, numTrials] = size(condMatrix);
    % Randomise the conditions
    shuffler = Shuffle(1:numTrials);
    condMatrixShuffled = condMatrix(:, shuffler);
    
    
    %----------------------------------------------------------------------
    %                     Make a response matrix
    %----------------------------------------------------------------------
    
    % This is a six row cell array:
    % the first row - the position of the cue in a given trial (1 = left, 2 = right),
    % the second row - the color of the cue in a given trial (1 = blue, 2 = red),
    % the third row - the box the participant moved to (1 = left, 2 = right),
    % the fourth row - the times the mouse position was taken
    % they fifth row - the x and y-coordinates of the leader mouse
    % the sixth row - the x and y-coordinates of the slave mouse
    respMat = cell(6, numTrials+1);
    
    %tic
    nEvents = 0;   % keep track of number of processed fieldtrip buffer events (not used)
    nSamples = 0;  % keep track of number of processed fieldtrip buffer samples thusfar
    
    % Start the trial loop
    while mousepos.trialnum <= numTrials
        
        % Reset screen to black
        Screen('FillRect', ptb.window, ptb.black);
        dontsync  = 1; % do not wait for v-sync
        
        % Position and color number
        posNum = condMatrixShuffled(1, mousepos.trialnum);
        colorNum = condMatrixShuffled(2, mousepos.trialnum);
        
        % The position of the cue and the color it is drawn in
        thePos = ptb.cueRects(posNum, :); %1=left, 2=right
        theColor = ptb.cueColors(colorNum, :); %1=blue, 2=red
        
        % The squares to draw in stage 3
        allRects = [ptb.boxLpos;ptb.boxRpos;thePos]';
        allColors = [ptb.gray; ptb.gray; theColor]';
        
        % Which computer is leading and saving the data
        leader = 'server';
        
        % The trajectory times and x,y-coordinates initialized for every trial
        points_leader = zeros(10000,2); %blue
        points_slave = zeros(10000,2); %red
        times = zeros(10000,1);
        
        % Initialize sample and stage
        sampleNum = 0;
        mousepos.stagenum = 1;
        
        % Determine whether a response has been made
        response = -1;
        leaderstart = false;
        slavestart = false;
        
        % Initialize timings
        stage1Start = Screen('Flip', ptb.window);
        stage1Deadline = stage1Start + ptb.deadlines(1) - ptb.halfFlip;
        % begintime = stage1Start
        nextsampletime = stage1Start;
        
        % The main trial loop
        while 1
            sampleNum = sampleNum + 1; % track which sample we're in
            times(sampleNum,1) = GetSecs; % get current sample's time
            nextsampletime = nextsampletime + ptb.increment; % check when to go to the next sample
            
            % get new remote mouse positions
            receive()
            if ~isempty(ret) && isstruct(ret)
                mousepos_remote = ret;
            end
            
            % send this computers mouse positions
            [x,y,buttons] = GetMouse(ptb.window);
            if mousepos.x ~= x || mousepos.y ~= y || ~isequal(mousepos.buttons,buttons)
                mousepos.x = x; mousepos.y = y; mousepos.buttons = buttons;
                send2buffer(mousepos);
            end
            
            % save all the coordinates
            points_leader(sampleNum,1) = x;
            points_leader(sampleNum,2) = y;
            points_slave(sampleNum,1) = mousepos_remote.x;
            points_slave(sampleNum,2) = mousepos_remote.y;
            
            
            if mousepos.stagenum == 1
                %------------------------------------------------------------------
                %                   Stage 1: Present the start box
                %------------------------------------------------------------------
                % Draw the start square. The start square stays on the screen
                % until the participant clicks it or 1,5 seconds pass.
                % If the deadline is missed, the next trial starts.
                
                Screen('FrameRect', ptb.window, ptb.red, ptb.startPos);
                % plot both cursors
                Screen('DrawTexture', ptb.window, texel, [], CenterRectOnPoint(spriteRect, mousepos_remote.x, mousepos_remote.y));
                Screen('DrawTexture', ptb.window, texel, [], CenterRectOnPoint(spriteRect, x, y));
                Screen('Flip', ptb.window, 0, 0, dontsync);
                
                % Check if both clicked the start button
                if strcmp(role,leader)
                    if (~leaderstart && any(buttons) && x>ptb.startPos(1) && x<ptb.startPos(3) && y>ptb.startPos(2) && y<ptb.startPos(4))
                        leaderstart = true;
                    end
                    if (~slavestart && any(mousepos_remote.buttons) && mousepos_remote.x>ptb.startPos(1) && mousepos_remote.x<ptb.startPos(3) ...
                            && mousepos_remote.y>ptb.startPos(2) && mousepos_remote.y<ptb.startPos(4))
                        slavestart = true;
                    end
                    
                    % If both clicked, advance to next stage
                    if leaderstart && slavestart
                        mousepos.stagenum = mousepos.stagenum+1;
                        send2buffer(mousepos); % update the other computer about the next stage
                        stage2Start = GetSecs();
                        stage2Deadline = stage2Start + ptb.deadlines(2) - ptb.halfFlip;
                    end
                    
                    % If the deadline is missed, go to the warning stage
                    if GetSecs >= stage1Deadline
                        mousepos.stagenum = 4;
                        send2buffer(mousepos);
                    end
                    
                    % If the slave computer, update stage when different
                else
                    if mousepos_remote.stagenum ~= mousepos.stagenum
                        mousepos.stagenum = mousepos_remote.stagenum;
                    end
                end
                
            elseif mousepos.stagenum == 2
                %------------------------------------------------------------------
                %                   Stage 2: Present the response boxes
                %------------------------------------------------------------------
                % Once they pressed the start button, show the response boxes
                % Wait for both participants to start moving by checking whether they have
                % moved by at least 5 pixels in the upward direction counting from the
                % boundary of the start button, i.e. if their position is above (lower than)
                % the coordinate y=820.
                
                Screen('FillRect', ptb.window, ptb.responseColors, ptb.responseRects);
                Screen('DrawTexture', ptb.window, texel, [], CenterRectOnPoint(spriteRect, mousepos_remote.x, mousepos_remote.y));
                Screen('DrawTexture', ptb.window, texel, [], CenterRectOnPoint(spriteRect, x, y));
                Screen('Flip', ptb.window, 0, 0, dontsync);
                
                if strcmp(role,leader)
                    if (y<820 && mousepos_remote.y<820)
                        mousepos.stagenum = mousepos.stagenum+1;
                        %                     % Figure out how long to wait until the next stage - doesn't work in this setup
                        %                     delay = 0.2 + (0.8-0.2).*rand(1,1);
                        %                     WaitSecs(delay);
                        send2buffer(mousepos);
                        stage3Start = GetSecs();
                        stage3Deadline = stage3Start + ptb.deadlines(3) - ptb.halfFlip;
                    end
                    
                    % Check if deadline missed
                    if GetSecs >= stage2Deadline
                        mousepos.stagenum = 4;
                        send2buffer(mousepos);
                    end
                    
                else % if the slave computer
                    if mousepos_remote.stagenum ~= mousepos.stagenum
                        mousepos.stagenum = mousepos_remote.stagenum;
                    end
                end
                
            elseif mousepos.stagenum == 3
                %------------------------------------------------------------------
                %                   Stage 3: Present cues
                %------------------------------------------------------------------
                % One person was asked to respond to the blue cue by moving to the left RB and clicking,
                % the other - to the red cue by moving to the right RB and clicking.
                % Draw the cue.
                % The trial ends after clicking on one of the response boxes with the deadline of 2s.
                
                Screen('FillRect', ptb.window, allColors, allRects);
                Screen('DrawTexture', ptb.window, texel, [], CenterRectOnPoint(spriteRect, mousepos_remote.x, mousepos_remote.y));
                Screen('DrawTexture', ptb.window, texel, [], CenterRectOnPoint(spriteRect, x, y));
                Screen('Flip', ptb.window, 0, 0, dontsync);
                
                if strcmp(role,leader)
                    % if it's blue's turn, we only go to the next trial when that participant responded (leader computer)
                    if colorNum == 1
                        % check if mouse was clicked
                        if any(buttons)
                            % check which box was clicked
                            if (x>ptb.boxLpos(1) && x<ptb.boxLpos(3) && y>ptb.boxLpos(2) && y<ptb.boxLpos(4))
                                response = 1;
                                mousepos.stagenum = mousepos.stagenum+1;
                                send2buffer(mousepos);
                                
                            elseif (x>ptb.boxRpos(1) && x<ptb.boxRpos(3) && y>ptb.boxRpos(2) && y<ptb.boxRpos(4))
                                response = 2;
                                mousepos.stagenum = mousepos.stagenum+1;
                                send2buffer(mousepos);
                            end
                        end
                        % if it's red's turn, we only go to the next trial when that participant responded (slave computer)
                    else % colorNum == 2
                        if any(mousepos_remote.buttons)
                            if (mousepos_remote.x>ptb.boxLpos(1) && mousepos_remote.x<ptb.boxLpos(3) ...
                                    && mousepos_remote.y>ptb.boxLpos(2) && mousepos_remote.y<ptb.boxLpos(4))
                                response = 1;
                                mousepos.stagenum = mousepos.stagenum+1;
                                send2buffer(mousepos);
                                
                            elseif (mousepos_remote.x>ptb.boxRpos(1) && mousepos_remote.x<ptb.boxRpos(3) ...
                                    && mousepos_remote.y>ptb.boxRpos(2) && mousepos_remote.y<ptb.boxRpos(4))
                                response = 2;
                                mousepos.stagenum = mousepos.stagenum+1;
                                send2buffer(mousepos);
                            end
                        end
                    end
                    
                    if GetSecs >= stage3Deadline
                        mousepos.stagenum = 4;
                        send2buffer(mousepos);
                    end
                    
                else % if the slave computer
                    if mousepos_remote.stagenum ~= mousepos.stagenum
                        mousepos.stagenum = mousepos_remote.stagenum;
                    end
                end
                
            else %mousepos.stagenum == 4
                %------------------------------------------------------------------
                %                   Stage 4: Present the warning screen
                %------------------------------------------------------------------
                % The whole screen is colored red if any of the time deadlines
                % is missed. It also shows in between trials.
                
                Screen('FillRect', ptb.window, ptb.red);
                Screen('Flip', ptb.window, 0, 0, dontsync);
                
                if strcmp(role,leader)
                    mousepos.trialnum = mousepos.trialnum+1;
                    mousepos.stagenum = 1;
                    WaitSecs(1);
                    send2buffer(mousepos);
                    break;
                    
                else % if the slave computer
                    if mousepos_remote.trialnum ~= mousepos.trialnum
                        mousepos.trialnum = mousepos_remote.trialnum;
                        break;
                    end
                end
                
            end
            
            % Go to the next sample
            while GetSecs<nextsampletime
                waitms = max(0,1000*(nextsampletime-GetSecs)/2);
                if waitms > 0.5, java.lang.Thread.sleep(waitms); end
            end
        end
        
        %----------------------------------------------------------------------
        %                     Save the data
        %----------------------------------------------------------------------
        
        if strcmp(role,leader)
            % convert all times to times relative to the start
            times = times(1:sampleNum,:);
            times = times - times(1);
            
            % save only the points that were sampled
            points_leader = points_leader(1:sampleNum,:);
            points_slave = points_slave(1:sampleNum,:);
            
            % Record the trial data into data matrix
            respMat{1, mousepos.trialnum} = posNum;
            respMat{2, mousepos.trialnum} = colorNum;
            respMat{3, mousepos.trialnum} = response;
            respMat{4, mousepos.trialnum} = times;
            respMat{5, mousepos.trialnum} = points_leader;
            respMat{6, mousepos.trialnum} = points_slave;
        end
        
    end
    
    %----------------------------------------------------------------------
    %                     Display end message
    %----------------------------------------------------------------------
    if ispc
        endInfo = 'Experiment finished. \n\n Press space to exit.';
        Screen('FillRect', ptb.window, ptb.black);
        
        DrawFormattedText(ptb.window, endInfo, 'center', 'center', ptb.white);
        Screen('Flip', ptb.window);
        
        [~, keyCode] = KbWait;
    end
    cleanup();
    
    
catch err
    disp(err.message);
    cleanup();
end


    function send2buffer(mp) % mp: mousepos
        %channels: role (server=1,client=2), mouse x-position, mouse y-position, trial number, stage number, button1, button2, button?
        mousedata = [rolenumber; mp.x; mp.y; mp.trialnum; mp.stagenum; mp.buttons(:)];
        sndClient.putData(mousedata(:),[size(mousedata,2) size(mousedata,1)]);
    end

    function receive
        ret = []; % init
        hdr = rcvClient.getHeader;
        % check new events (Note: events not used)
        if hdr.nEvents > nEvents
            % get the last event for this process
            evts = rcvClient.getEvents(nEvents,hdr.nEvents-1); % only pick last one
            for n = numel(evts) : -1: 1
                if ~strcmp(char(evts(n).getType),role)
                    ret=evts(n).getValue.getArray; % Note: 1-based counting here!!
                    break;
                end
            end
            nEvents = hdr.nEvents;
        end
        % check new incoming data
        if hdr.nSamples > nSamples
            % get last data sample
            % only pick the last data sample from the remote buffer, it holds the most recent content/info
            data = rcvClient.getDoubleData(hdr.nSamples-1,hdr.nSamples-1);
            if ~isempty(data)
                ret = struct('role',roles{data(end,ROLE)}, ...
                    'x',data(end,XPOS),'y',data(end,YPOS), ...
                    'trialnum',data(end,TRIAL),'stagenum',data(end,STAGE), ...
                    'buttons',logical(data(end,STAGE+1:end)));
                % disp(ret);
            end
            nSamples = hdr.nSamples;
        end
    end

    function install_fieldtrip_server()
        % Initiate FieldTrip buffer server
        bufferServer.BufferServerStart(svport,120000,500);
        % connect client to this FieldTrip buffer to receive remote mouse info
        sndClient.connect('localhost',svport);
        % Start buffer with correct header, determine number of required channels
        [~,~,buttons] = GetMouse(ptb.window);
        numChannels = STAGE + numel(buttons); % 1=role,3=xpos,4=ypos,5=trial,6=stage,7=btn1,8=btn2,9=btn3,....
        Fs = 1; % not meaningful (intermittent data, depends on mouse movements)
        dataType = 10; % FLOAT32
        import('nl.fcdonders.fieldtrip.bufferclient.Header');
        hdr=Header(numChannels,Fs,dataType);
        sndClient.putHeader(hdr);
    end

    function connect_client()
        % Connect client to remote FieldTrip buffer for sending this mouse info to remote computer
        fprintf('Waiting for FieldTrip buffer...');
        while 1
            try
                rcvClient.connect(ip,clport);
            catch
                fprintf('.');
                pause(0.5);
                continue
            end
            fprintf('\n');
            break % we're connected
        end
        while 1
            try
                rcvClient.getHeader();
            catch
                fprintf(':');
                pause(0.5);
                continue
            end
            fprintf('\n');
            break % header defined
        end
    end

    function cleanup()
        try closePTB(); catch, end
        try psychrethrow(psychlasterror); catch, end
        stop_buffer();
    end

    function stop_buffer()
        if exist('bufferServer','var') && ~isempty(bufferServer)
            try bufferServer.stopBuffer(); catch, end
            bufferServer = [];
        end
        % get object to access static list of running fieldtrip servers
        import nl.fcdonders.fieldtrip.bufferserver.BufferServer
        srv = BufferServer;
        srv.removeAllServerSockets;
    end

end