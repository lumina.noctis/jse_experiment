function plotTrajectories(respMat, trialNum, mode, fromSample, toSample)
  % given a particular response matrix respMat
  % and a trial number trialNum
  % make plots of mouse trajectories for single participant ('single' mode)
  % or two participants in the same figure ('double' mode)

  numSamples = length(respMat(trialNum).xleader);

  screenWidth = 1920;
  screenHeight = 1080;
  if nargin < 4
    fromSample = 1;
    toSample = numSamples;
  end
  
  if strcmp(fromSample,'startClick')
    fromSample = find(respMat(trialNum).times > respMat(trialNum).startTime, 1);
    toSample = numSamples;
  end
  
  if toSample > numSamples
    error('Your toSample argument has to be at most %d', numSamples);
  end

  if strcmp(mode,'single')
    points = respMat{5, trialNum};
    % Plot the contour in a Matlab figure
    plot(points(fromSample:toSample,1), 900-points(:,2));
    xlim([0,screenWidth]);
    ylim([0,screenHeight]);
  elseif strcmp(mode,'double')
    pointsA = [respMat(trialNum).xleader, respMat(trialNum).yleader];
    pointsB = [respMat(trialNum).xslave, respMat(trialNum).yslave];
    % Plot the contour in a Matlab figure
    %hold off;
    figure(1)
    plot(pointsA(fromSample:toSample,1),screenHeight-pointsA(fromSample:toSample,2),'r');
    hold on;
    plot(pointsB(fromSample:toSample,1),screenHeight-pointsB(fromSample:toSample,2),'b');
    xlim([0,screenWidth]);
    ylim([0,screenHeight]);
  end
  
end