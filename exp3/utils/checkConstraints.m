function allcounts = checkConstraints(trials)
    count11 = zeros(1,4);
    count12 = zeros(1,4);
    count21 = zeros(1,4);
    count22 = zeros(1,4);

    for m=2:length(trials)
        prev = trials(m-1,:);
        curr = trials(m,:);
        if isequal(prev, [1,1])
            if isequal(curr,[1,1])
                count11(1,1) = count11(1,1) + 1;
            elseif isequal(curr,[1,2])
                count11(1,2) = count11(1,2) + 1;
            elseif isequal(curr,[2,1])
                count11(1,3) = count11(1,3) + 1;
            elseif isequal(curr,[2,2])
                count11(1,4) = count11(1,4) + 1;
            end
        elseif isequal(prev, [1,2])
            if isequal(curr,[1,1])
                count12(1,1) = count12(1,1) + 1;
            elseif isequal(curr,[1,2])
                count12(1,2) = count12(1,2) + 1;
            elseif isequal(curr,[2,1])
                count12(1,3) = count12(1,3) + 1;
            elseif isequal(curr,[2,2])
                count12(1,4) = count12(1,4) + 1;
            end
        elseif isequal(prev, [2,1])
            if isequal(curr,[1,1])
                count21(1,1) = count21(1,1) + 1;
            elseif isequal(curr,[1,2])
                count21(1,2) = count21(1,2) + 1;
            elseif isequal(curr,[2,1])
                count21(1,3) = count21(1,3) + 1;
            elseif isequal(curr,[2,2])
                count21(1,4) = count21(1,4) + 1;
            end
        elseif isequal(prev, [2,2]) 
            if isequal(curr,[1,1])
                count22(1,1) = count22(1,1) + 1;
            elseif isequal(curr,[1,2])
                count22(1,2) = count22(1,2) + 1;
            elseif isequal(curr,[2,1])
                count22(1,3) = count22(1,3) + 1;
            elseif isequal(curr,[2,2])
                count22(1,4) = count22(1,4) + 1;
            end
        end
    end
    allcounts = [count11;count12;count21;count22];
end
