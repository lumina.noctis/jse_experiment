told3 = load('/Users/katja/KatjaCCS3/Data/trialorderExp3.mat');
tnew3 = load('/Users/katja/matlabwd/KatjaCCS3/Data/trialorderExp3.mat');

told4 = load('/Users/katja/KatjaCCS3/Data/trialorderExp4.mat');
tnew4 = load('/Users/katja/matlabwd/KatjaCCS3/Data/trialorderExp4.mat');

for i=11:20
    told3.trialMatrix3(i) = tnew3.trialMatrix3(i);
    told4.trialMatrix4(i) = tnew4.trialMatrix4(i);
end

trialMatrix3 = told3.trialMatrix3;
trialMatrix4 = told4.trialMatrix4;

save('updatedTrial3.mat', 'trialMatrix3');
save('updatedTrial4.mat', 'trialMatrix4');