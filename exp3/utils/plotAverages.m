function grandMeans = plotAverages
width = 1920;
height = 1080;

filename = sprintf('/Users/katja/matlabwd/KatjaCCS/Data/Experiment1/Responses_%i.mat', 1);
r = importdata(filename);
condMeans = transformPoints(r);
grandMeans = condMeans;

for subj=2:20
    filename = sprintf('/Users/katja/matlabwd/KatjaCCS/Data/Experiment1/Responses_%i.mat', subj);
    r = importdata(filename);
    condMeans = transformPoints(r);
    grandMeans = (grandMeans + condMeans)./2;
end

figure(1)
hold on
plot(grandMeans(40:101,1), grandMeans(40:101,5), '-b');
plot(grandMeans(40:101,2), grandMeans(40:101,6), ':b');
plot(grandMeans(40:101,3), grandMeans(40:101,7), '-r');
plot(grandMeans(40:101,4), grandMeans(40:101,8), ':r');
xlim([-width/2,width/2]);
ylim([0,height]);

end