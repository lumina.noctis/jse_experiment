function ptb = initPTB_simon(block)

addpath(genpath('/Applications/Psychtoolbox/'));

try
    %----------------------------------------------------------------------
    %                     Set up toolbox
    %----------------------------------------------------------------------

    % Clear the workspace
    close all;
    sca;

    %%%%%%%%% for testing on main screen (fails otherwise)
    %Screen('Preference', 'SkipSyncTests', 1);

    % Setup PTB with some default values
    PsychDefaultSetup(2);

    % Prepare setup of imaging pipeline for onscreen window
    PsychImaging('PrepareConfiguration');

    % Set the screen number to the external secondary monitor if there is one
    % connected
    ptb.screenNumber = max(Screen('Screens'));

    % Define colors
    ptb.white = WhiteIndex(ptb.screenNumber);
    ptb.black = BlackIndex(ptb.screenNumber);
    ptb.gray = [0.5 0.5 0.5];
    ptb.red = [1 0 0];
    ptb.blue = [0 0 1];

    % Open the screen
    [ptb.window, ptb.windowRect] = PsychImaging('OpenWindow', ptb.screenNumber, ptb.black, [], 32, 2);
    %[ptb.window, ptb.windowRect] = PsychImaging('OpenWindow', ptb.screenNumber, ptb.black, [0 0 800 600], 32, 2);

    % Get the size of the on screen window in pixels.
    [screenWidth, screenHeight] = Screen('WindowSize', ptb.window);
    % Get the centre coordinate of the window in pixels
    [xCenter, yCenter] = RectCenter(ptb.windowRect);

    % Set the blend funciton for the screen
    Screen('BlendFunction', ptb.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

    % Query the maximum priority level
    topPriorityLevel = MaxPriority(ptb.window);
    Priority(topPriorityLevel);

    % Flip to clear
    Screen('Flip', ptb.window);

    % Set the text size
    Screen('TextSize', ptb.window, 24);
    Screen('TextFont', ptb.window, 'Times');


    %----------------------------------------------------------------------
    %                       Timing Information
    %----------------------------------------------------------------------

    % Query the frame duration, inter-frame interval
    ptb.halfFlip = Screen('GetFlipInterval', ptb.window)/2;
    desiredSampleRate = 92; % number of samples per sec
    ptb.increment = 1/desiredSampleRate;
    %ptb.deadlines = [5, 5, 5];
    ptb.deadlines = [1.5, 1.5, 2];


    %----------------------------------------------------------------------
    %                     Make the experiment layout
    %----------------------------------------------------------------------

    % Relative sizes from Scherbaum's experiment look bad:
    %      relative_cuesize = 0.27;
    %      relative_cuedist = 0.27;
    %      relative_boxsize = 0.37;

    % New relative sizes
    relative_cuesize = 0.08;
    relative_cuedist = 0.15;
    relative_boxsize = 0.2;
    relative_startsize = 0.08;
    relative_startdist = 0.45;


    % Make two response boxes, symmetric on top of the screen
    boxWidth = screenWidth * relative_boxsize;
    boxHeight = boxWidth * 1/3;
    %	ptb.boxSize = [0 0 300 150];
    ptb.boxSize = [0 0 boxWidth boxHeight];
    ptb.boxLpos = CenterRectOnPointd(ptb.boxSize, ptb.boxSize(3)/2, ptb.boxSize(4)/2);
    ptb.boxRpos = CenterRectOnPointd(ptb.boxSize, screenWidth - ptb.boxSize(3)/2, ptb.boxSize(4)/2);

    % Make two cue squares, which will be placed on top of the response boxes
    %ptb.cueSize = [0 0 100 100];
    cueWidth = screenWidth * relative_cuesize;
    cueHeight = cueWidth;
    % cueDist = 100;
    cueDist = screenWidth * relative_cuedist;
    ptb.cueSize = [0 0 cueWidth cueHeight];
    ptb.cueLpos = CenterRectOnPointd(ptb.cueSize, screenWidth/2 - cueDist, yCenter); % left cue
    ptb.cueRpos = CenterRectOnPointd(ptb.cueSize, screenWidth/2 + cueDist, yCenter); % right cue

    % Make a start rectangle.
    % ptb.startSize = [0 0 100 50];
    startWidth = screenWidth * relative_startsize;
    startHeight = startWidth/2;
    startDist = screenHeight * relative_startdist;
    ptb.startSize = [0 0 startWidth startHeight];
    ptb.startPos = CenterRectOnPointd(ptb.startSize, xCenter, yCenter+startDist);

    ptb.cueRects = [ptb.cueLpos; ptb.cueRpos];

    if block == 1
        ptb.cueColors = [ptb.blue; ptb.red];
    elseif block == 2
        ptb.cueColors = [ptb.red; ptb.blue];
    else
        error('Block should be either 1 or 2');
    end

    ptb.responseRects = [ptb.boxLpos; ptb.boxRpos]';
    ptb.responseColors = [ptb.gray; ptb.gray]';


    %----------------------------------------------------------------------
    %                       Keyboard information
    %----------------------------------------------------------------------

    KbName('UnifyKeyNames');
    ptb.rightarrow = KbName('RightArrow');
    ptb.leftarrow = KbName('LeftArrow');
    ptb.space = KbName('space');
    %ListenChar(2);


catch
    closePTB();
    psychrethrow(psychlasterror);
end
    
end