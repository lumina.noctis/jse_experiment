function server = jslisten(port)
% get listener server socket
import java.nio.*
import java.net.*

%Create server socket connection
try
	server = java.nio.channels.ServerSocketChannel.open();
	server.socket().bind(java.net.InetSocketAddress(port));
	server.configureBlocking(false);
catch err
	fprintf('%s\n',err.message);
	server = -1;
	return
end


