function success = jssend(socket,var)

% serialize
var = int8(int16(msserialize(var))-128);

% send it
success = jswrite(socket,var);