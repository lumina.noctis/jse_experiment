%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The BrainStream software is free but copyrighted software, distributed  %
% under the terms of the GNU General Public Licence as published by       %
% the Free Software Foundation (either version 2, or at your option       %
% any later version). See the file COPYING.txt in the main BrainStream    %
% folder for more details.                                                %
%                                                                         %
% Copyright (C) 2009, Philip van den Broek                                %
% Donders Institute for Brain, Cognition and Behaviour                    %
% Radboud University Nijmegen, The Netherlands                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function pnames = adaptPath(pnames)
reppathsep = char(setdiff({'\','/'},filesep));
if ischar(pnames)
	if ~isURL(pnames)		
		pnames = strrep(pnames,reppathsep,filesep);
	end
end
if iscell(pnames)
	for n = 1 : numel(pnames)
		if ~isURL(pnames{n})		
			pnames{n} = strrep(pnames{n},reppathsep,filesep);
		end
	end
end
function ret = isURL(url)
	ret = false;
	if any(findstr(url,'://')), ret = true; end
end
end
