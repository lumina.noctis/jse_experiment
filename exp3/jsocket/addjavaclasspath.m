%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The BrainStream software is free but copyrighted software, distributed  %
% under the terms of the GNU General Public Licence as published by       %
% the Free Software Foundation (either version 2, or at your option       %
% any later version). See the file COPYING.txt in the main BrainStream    %
% folder for more details.                                                %
%                                                                         %
% Copyright (C) 2009, Philip van den Broek                                %
% Donders Institute for Brain, Cognition and Behaviour                    %
% Radboud University Nijmegen, The Netherlands                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function javapath = addjavaclasspath(p)
if ischar(p), p=cellstr(p); end
javapath = javaclasspath; % init
% remove already installed items
toremove = [];
for n = 1 : numel(p)
	[p1 n1 e1] = fileparts(p{n});
	name1 = [n1 e1];
	for j = 1 : numel(javapath)
		[p2 n2 e2] = fileparts(javapath{j});
		name2 = [n2 e2];
		if strcmp(name1,name2)
			toremove = horzcat(toremove,n);
		end
	end
% 	if strmatch(p{n},javapath)
% 		toremove = horzcat(toremove,n);
% 	end
end
p(toremove) = [];
if ~isempty(p)
	javaaddpath(adaptPath(p));
end
