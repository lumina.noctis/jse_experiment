function success = jswrite(socket,data)

import java.nio.*

success = -1;
try
% send something
buf = java.nio.ByteBuffer.allocate(numel(data));
buf.put(data);
buf.flip();

num = 0;
while(buf.hasRemaining()) 
    num = num + socket.write(buf);
end
catch err
	fprintf('%s\n',err.message);
	return
end
success = num;
