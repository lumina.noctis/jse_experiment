function [var, success] = jsrecv(socket,timeout)
[var, success] = jsread(socket,timeout);
if success > 0
	var = msdeserialize(uint8(int16(var)+128));	
end