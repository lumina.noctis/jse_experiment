function [socket, hostip, hostname] = jsaccept(server,timeout)
% accept a single incoming client request
if nargin < 2, timeout = inf; end

import java.nio.*
import java.net.*

hostip = [];
hostname = [];
socket = [];

try
	tic
	while 1
		if ~server.isOpen
			error('Server not binded to any port')
		end
		socket = server.accept();
		if ~isempty(socket)
			hostip = socket.socket.getInetAddress.getHostAddress.toString;
			hostname = socket.socket.getInetAddress.getCanonicalHostName.toString;
			break
		end
		if toc > timeout
			socket = -1;
			break
		else
			WaitSecs(0.0005);
		end
	end
catch err
	fprintf('%s\n',err.message);	
end
if ~isempty(server)
	server.close; 
end