function [socket] = jsconnect(ip,port)
% connect client to server

import java.nio.*
import java.net.*

%Create socket connection
try
	socket = java.nio.channels.SocketChannel.open();
	socket.configureBlocking(false);
	socket.connect(java.net.InetSocketAddress(ip, port));
	while(~socket.finishConnect() )
		pause(0.01); % wait
	end
catch err
	fprintf('%s\n',err.message);
	return
end


