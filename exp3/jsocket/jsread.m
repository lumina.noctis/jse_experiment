function [var, success] = jsread(socket,timeout)
var = [];
% reading from socket

buf = java.nio.ByteBuffer.allocate(1024);

twait = 0.0005;
tic
while 1
	num = socket.read(buf);
	if num <= 0 && isempty(var)
		if toc > timeout-twait
			break
		else
			WaitSecs(twait);
			continue
		end
	end
	if num <= 0, break; end
	buf.flip();
	offset = numel(var);
	var(1,end+1:end+num) = int8(0);
	for n = 1 : num
		var(offset+n) = buf.get();
	end
	buf.clear();
end
if ~isempty(var)
	success = numel(var);
else
	success = -1;
end