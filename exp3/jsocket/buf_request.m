function success = buf_request(socket)

import java.nio.*
import java.net.*

%function success = jssend(socket,var)

% % request header (GET_HDR)
% buf = java.nio.ByteBuffer.allocate(8);
% VER = int16(1);
% CMD = int16(hex2dec('201')); % GET_HDR
% buf.putShort(VER).putShort(CMD).putInt(0).rewind();

VER		= int16(1);
CMD		= int16(hex2dec('402')); % WAIT_DAT
nSamples = int32(2^32-1);
nEvents	= int32(1);
timeout	= int32(10000); % 10s
buf = java.nio.ByteBuffer.allocate(20);
buf.putShort(VER).putShort(CMD).putInt(12);
buf.putInt(nSamples).putInt(nEvents).putInt(timeout).rewind();

% send to buffer
success = -1;
try	
	num = 0;
	while(buf.hasRemaining())
		num = num + socket.write(buf);
	end
catch err
	fprintf('%s\n',err.message);
	return
end
success = num;
