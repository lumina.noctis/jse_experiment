% install java FieldTrip buffer classes
javaaddpath(which('ft_bufferserver.jar'));
javaaddpath(which('BufferClient.jar'));

% import java classes
import nl.fcdonders.fieldtrip.bufferserver.*
import nl.fcdonders.fieldtrip.bufferclient.*
