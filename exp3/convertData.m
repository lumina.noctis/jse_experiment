function convertData(subStart, subEnd, folderName)

data_path = [pwd sprintf('/Data/%s', folderName)];
addpath(data_path);

results_path = [pwd sprintf('/Results/%s/', folderName)];

for i=subStart:subEnd
    data2csv(i);
end


function data2csv(subjNum)

dataFile = sprintf('Responses_%i.mat', subjNum);
respMat = importdata(dataFile);

[~, ncols] = size(respMat);

subjNum1 = sprintf('%03d', subjNum*2-1);
subjNum2 = sprintf('%03d', subjNum*2);

for col = 1:ncols
    respMat(col).trial = col;
    if respMat(col).cueColor == 1
        respMat(col).x = respMat(col).xleader;
        respMat(col).y = respMat(col).yleader;
        respMat(col).subid = subjNum1;
    else
        respMat(col).x = respMat(col).xslave;
        respMat(col).y = respMat(col).yslave;        
        respMat(col).subid = subjNum2;
    end
end

temp_table = struct2table(respMat);

% this keeps all columns: x, y, xleader, yleader, xslave, yslave
%temp_table = temp_table(:,[12, 15, 1:7, 13:14, 8:11]);
% this keeps only x, y columns that belong to the participant responding
% in the given trial
temp_table = temp_table(:,[12, 15, 1:7, 13:14]);


csvname1 = sprintf(strcat(results_path, 's%s.csv'), subjNum1);
csvname2 = sprintf(strcat(results_path, 's%s.csv'), subjNum2);

writetable(temp_table(strcmp(temp_table.subid, subjNum1), :), csvname1);
writetable(temp_table(strcmp(temp_table.subid, subjNum2), :), csvname2);

end

end