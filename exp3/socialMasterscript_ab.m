function socialMasterscript_ab(mode, subjectNum, role, ip, port)

% This is a masterscript for the Mouse-tracking Social Simon task experiment.
% It runs the whole experiment and saves the data.
% This "pr" version is one, where the other's cursor is visible.
% mode (str): experimental mode, 'demo' or 'test'
% block (int): block, 1 = blue left, red right, 2 = blue right, red left
% subjectNum (int): the participant number used for data storage;
%                   in the social conditions, the number refers to the 
%                   pair of subjects
% role, ip, port: required for two-computer connection

addpath(genpath('/Applications/Psychtoolbox/'));
% cd('/Users/katja/matlabwd/twomice')

% set data folder:
data_path = [pwd '/Data/'];
addpath(data_path);

%----------------------------------------------------------------------
%                     Psychtoolbox housekeeping
%----------------------------------------------------------------------
% get old Psychtoolbox preferences
oldVisualDebugLevel = Screen('Preference', 'VisualDebugLevel');
oldSupressAllWarnings = Screen('Preference', 'SuppressAllWarnings');
oldSyncTests = Screen('Preference', 'SkipSyncTests');

% set new preferences
Screen('Preference', 'VisualDebugLevel', 3);
Screen('Preference', 'SuppressAllWarnings', 1);
Screen('Preference', 'SkipSyncTests', 1);


%----------------------------------------------------------------------
%                     Connection prerequisites
%----------------------------------------------------------------------

persistent bufferServer
% install java FieldTrip buffer classes
addjavaclasspath(which('ft_bufferserver.jar'));
addjavaclasspath(which('BufferClient.jar'));

% import java classes
import nl.fcdonders.fieldtrip.bufferserver.*
import nl.fcdonders.fieldtrip.bufferclient.*

% make sure to stop possible earlier started buffer
stop_buffer();

% add required folders to Matlab path
thisfilespath = fileparts(mfilename('fullpath'));
addpath(genpath(thisfilespath));

% make sure cleanup procesdure also executes after breaking out with ctrl-c
clean = onCleanup(@()cleanup()); % executes at cleanup of local variable clean


%----------------------------------------------------------------------
%                     Required files
%----------------------------------------------------------------------
practiceorderFileDemo = strcat(data_path, 'practiceorderExp4Demo.mat');
trialorderFileDemo = strcat(data_path, 'trialorderExp4Demo.mat');
practiceorderFile = strcat(data_path, 'practiceorderExp4.mat');
trialorderFile = strcat(data_path, 'trialorderExp4.mat');

practiceTrialMatrixDemo = importdata(practiceorderFileDemo);
trialMatrixDemo = importdata(trialorderFileDemo);

practiceTrialMatrix = importdata(practiceorderFile);
trialMatrix = importdata(trialorderFile);

cond_path = strcat(data_path, 'Experiment4/');        

blockOrderFile = strcat(data_path, 'blockOrderExp4.mat');
blockOrder = importdata(blockOrderFile);

block = blockOrder(subjectNum);

if strcmp(mode, 'demo')
    practiceMatrix = practiceTrialMatrixDemo{1,1};
    expMatrix = trialMatrixDemo{subjectNum};
elseif strcmp(mode, 'test')
    practiceMatrix = practiceTrialMatrix{1,1};
    expMatrix = trialMatrix{subjectNum};
else
    error('Mode should be either ''demo'' or ''test''');
end

[numPracticeTrials, ~] = size(practiceMatrix);

round1End = numPracticeTrials/4;
round2End = numPracticeTrials/4 * 2;

%----------------------------------------------------------------------
%                     Required files
%----------------------------------------------------------------------

if block == 1
    startInfo1_p1 = fileread('nonvis_soc_simon_instructions1_p1b1.txt');
    startInfo1_p2 = fileread('nonvis_soc_simon_instructions1_p2b1.txt');
elseif block == 2
    startInfo1_p1 = fileread('nonvis_soc_simon_instructions1_p1b2.txt');
    startInfo1_p2 = fileread('nonvis_soc_simon_instructions1_p2b2.txt');
else
    error('Block should be either 1 or 2');
end

startInfo2 = fileread('soc_simon_instructions2.txt');
round1Msg = ['Practice Round 1 involves no time deadlines.\n\n',...
        'After every trial you will be given feedback on whether your response\n',...
        'was correct and how much time you took to complete the trial\n\n',...
        'Click on the gray square at the bottom of the page to begin the Practice Round 1.'];
round2Msg = ['Practice Round 2 involves time deadlines.\n\n',...
        'You have 1.5 seconds to click on the start button at the bottom of the screen,\n',...
        '1.5 seconds to start moving upwards and 2 seconds to click on\n',...
        'one of the response boxes.\n\n',...
        'If you miss any of the deadlines, you will see a message "Missed deadline!"\n',...
        'After every completed trial you will be given feedback on whether your response\n',...
        'was correct and how much time you took to complete the trial.\n\n',...
        'Click on the gray square at the bottom of the page to begin the Practice Round 2.'];
round3Msg = ['Practice Round 3 involves time deadlines.\n\n',...
        'You will be given no feedback on your performance.\n\n',...
        'Click on the gray square at the bottom of the page to begin the Practice Round 3.'];
interInfo = (['You have completed the practice rounds.\n\n',...
    'Click on the gray square at the bottom of the page to begin the experiment']);
breakMsg = ['You may take a short break now. \n\n',...
    'Click on the gray square at the bottom of the page to continue.'];
halfMsg = ['You have completed half of the trials. \n',...
    'You may take a short break now.\n\n',...
    'Click on the gray square at the bottom of the page to continue.'];



try
    % initialize Psychtoolbox
    ptb = initPTB_simon(block);

    % Create sprites for both mouse pointers
    imgFile1 = 'hand1.png';

    [image1, ~, alpha1] = imread(imgFile1);
    
    image1(:,:,4) = alpha1(:,:);
    
    texel1 = Screen('MakeTexture', ptb.window, image1);
    
    [s1,s2,~] = size(image1);
    imgH = s1 * 0.05;
    imgW = s2 * 0.05;
    spriteRect = [0 0 imgW imgH]; % The bounding box for the sprite
    
    HideCursor(ptb.window);

    % data channels: role (server=1,client=2), mouse x-position, 
    % mouse y-position, trial number, stage number, button1, button2, button?
    % channel index constants into mouse data matrix
    ROLE=1;
    XPOS=2; % x-channel position in mouse data matrix
    YPOS=3; % y-channel position in mouse data matrix
    TRIAL=4;
    STAGE=5;
    PHASE=6;
    RESPONSE=7;
    BRK=8;
    roles={'server','client'};

    % Get FieldTrip buffer server object
    bufferServer=BufferServer();
    % Get client objects that connect to both fieldtrip buffers
    sndClient=BufferClient; % client for sending mouse data to remote computer
    rcvClient=BufferClient; % client for receiving mouse data from remote computer
    
    if strcmp(role,'server')
        rolenumber = 1;
        svport = port;
        clport = port+1;
    elseif strcmp(role,'client')
        rolenumber = 2;
        svport = port+1;
        clport = port;
    else
        error('Role should be either ''server'' or ''client''');
    end
    
    % Install FieldTrip server
    install_fieldtrip_server(); % install FieldTrip buffer server and connect sndClient to it
    connect_client();   % connect rcvClient to remote computer FieldTrip buffer server
    fprintf('\nConnected to FieldTrip buffer\n');
    
    % Initialize two structs for information exchange between computers
    mousepos = struct('iam',role,'x',0,'y',0,'buttons',[],'trialnum',1,...
        'stagenum',1,'phasenum',1,'response',-1, 'brk', 0);
    mousepos_remote = struct('iam',roles{~strcmp(roles,role)},'x',0,'y',...
        0,'buttons',[],'trialnum',1,'stagenum',1,'phasenum',1,'response',-1,...
        'brk', 0);

    nEvents = 0;   % keep track of number of processed fieldtrip buffer events (not used)
    nSamples = 0;  % keep track of number of processed fieldtrip buffer samples thusfar

    leader = 'server';
    textpage = 1;
    
    
    while 1
        
        if mousepos.phasenum == 1
            % Display instructions and wait for both to be ready
            if textpage == 1
                % get new remote mouse positions
                receive()
                if ~isempty(ret) && isstruct(ret)
                    mousepos_remote = ret;
                end

                % send this computers mouse positions
                [x,y,buttons] = GetMouse(ptb.window);
                if mousepos.x ~= x || mousepos.y ~= y || ~isequal(mousepos.buttons,buttons)
                    mousepos.x = x; mousepos.y = y; mousepos.buttons = buttons;
                    send2buffer(mousepos);
                end

                % Present a start screen and wait for a key-press
                if strcmp(role, leader)
                    DrawFormattedText(ptb.window, startInfo1_p1, 150, 50, ptb.white, [], [], [], 1.25);
                else
                    DrawFormattedText(ptb.window, startInfo1_p2, 150, 50, ptb.white, [], [], [], 1.25);
                end
                
                Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                Screen('Flip', ptb.window);

                [~, keyCode] = KbWait;
                if keyCode(ptb.space)
                  textpage = 2;
                  leaderdone = false;
                  slavedone = false;
                end
            end

            if textpage == 2
                displayJointMsg(startInfo2);
            end

        elseif mousepos.phasenum == 2
            % Display practice round 1 message
            displayJointMsg(round1Msg);
                
        elseif mousepos.phasenum == 3
            % Run practice round 1
            practiceRound1(practiceMatrix(1:round1End,:));            
            mousepos.phasenum = mousepos.phasenum + 1;
            
        elseif mousepos.phasenum == 4
            % Display practice round 2 message
            displayJointMsg(round2Msg);

        elseif mousepos.phasenum == 5
            % Run practice round 2
            practiceRound2(practiceMatrix(round1End+1:round2End,:));            
            mousepos.phasenum = mousepos.phasenum + 1;
            
        elseif mousepos.phasenum == 6
            % Display practice round 3 message
            displayJointMsg(round3Msg);

        elseif mousepos.phasenum == 7
            % Run practice round 3
            practiceRound3(practiceMatrix(round2End+1:numPracticeTrials,:));
            mousepos.phasenum = mousepos.phasenum + 1;
            
        elseif mousepos.phasenum == 8
            % Display intermediate message
            displayJointMsg(interInfo);

        elseif mousepos.phasenum == 9
            % Run experiment
            responses = experiment(expMatrix);
            % Save data
            if strcmp(role,leader)
                if strcmp(mode, 'demo')
                    responsesFile = sprintf('DemoResponses_%i.mat', subjectNum);
                else
                    responsesFile = sprintf('Responses_%i.mat', subjectNum);
                end
                save(strcat(cond_path, responsesFile), 'responses');
            end
            mousepos.phasenum = mousepos.phasenum + 1;
        else
            endInfo = 'Experiment finished. \n\n Press space to exit.';
            while 1
                % Present a message and wait for a key-press
                Screen('FillRect', ptb.window, ptb.black);
                DrawFormattedText(ptb.window, endInfo, 'center', 'center', ptb.white, [], [], [], 1.25);
                Screen('Flip', ptb.window);

                [~, keyCode] = KbWait;
                if keyCode(ptb.space)
                  break;
                end
            end
            cleanup();            
        end
    end


catch err
    disp(err.message);
    cleanup();
end

%----------------------------------------------------------------------
%                     Psychtoolbox housekeeping
%----------------------------------------------------------------------
% Restore preferences
Screen('Preference', 'VisualDebugLevel', oldVisualDebugLevel);
Screen('Preference', 'SuppressAllWarnings', oldSupressAllWarnings);
Screen('Preference', 'SkipSyncTests', oldSyncTests);


    % All the functions used above
    function displayJointMsg(msg)
        % Present a message screen and wait for clicking on the "ready" button

        while 1
            % get new remote mouse positions
            receive()
            if ~isempty(ret) && isstruct(ret)
                mousepos_remote = ret;
            end

            % send this computers mouse positions
            [x,y,buttons] = GetMouse(ptb.window);
            if mousepos.x ~= x || mousepos.y ~= y || ~isequal(mousepos.buttons,buttons)
                mousepos.x = x; mousepos.y = y; mousepos.buttons = buttons;
                send2buffer(mousepos);
            end
            
            Screen('FillRect', ptb.window, ptb.gray, ptb.startPos);
            DrawFormattedText(ptb.window, msg, 150, 50, ptb.white, [], [], [], 1.25);
            Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
            Screen('Flip', ptb.window);

            if strcmp(role,leader)
                if (~leaderdone && any(buttons) && x>ptb.startPos(1) && x<ptb.startPos(3) ...
                        && y>ptb.startPos(2) && y<ptb.startPos(4))
                    leaderdone = true;
                end
                if (~slavedone && any(mousepos_remote.buttons) && mousepos_remote.x>ptb.startPos(1) ...
                        && mousepos_remote.x<ptb.startPos(3) && mousepos_remote.y>ptb.startPos(2) ...
                        && mousepos_remote.y<ptb.startPos(4))
                    slavedone = true;
                end

                % If both clicked on the ready button, advance to next phase
                if leaderdone && slavedone
                    while any(buttons) || any(mousepos_remote.buttons)
                        % get new remote mouse positions
                        receive()
                        if ~isempty(ret) && isstruct(ret)
                            mousepos_remote = ret;
                        end
                        [~,~,buttons] = GetMouse(ptb.window);
                    end

                    leaderdone = false;
                    slavedone = false;
                    mousepos.phasenum = mousepos.phasenum+1;
                    send2buffer(mousepos);
                    break;
                end

            else
                if mousepos_remote.phasenum > mousepos.phasenum
                    mousepos.phasenum = mousepos_remote.phasenum;
                    break;
                end
            end
        end
    end


    function practiceRound1(trials)
        [numtr, ~] = size(trials);
        while mousepos.trialnum <= numtr

            Screen('FillRect', ptb.window, ptb.black);

            % Position and color number
            posNum = trials(mousepos.trialnum, 1);
            colorNum = trials(mousepos.trialnum, 2);

            % The position of the cue and the color it is drawn in
            thePos = ptb.cueRects(posNum, :); %1=left, 2=right
            theColor = ptb.cueColors(colorNum, :); %1=blue, 2=red

            % The squares to draw in stage 3
            allRects = [ptb.boxLpos;ptb.boxRpos;thePos]';
            allColors = [ptb.gray; ptb.gray; theColor]';

            % The trajectory times and x,y-coordinates initialized for every trial
            sampleNum = 0;
            mousepos.stagenum = 1;

            % Determine whether a response has been made
            dontsync  = 1; % 1: do not wait for v-sync
            leaderstart = false;
            slavestart = false;

            % Initialize timings
            stage1Start = Screen('Flip', ptb.window);
            nextsampletime = stage1Start;

            while 1        

                sampleNum = sampleNum + 1;	
                nextsampletime = nextsampletime + ptb.increment;

                % get new remote mouse positions
                receive()
                if ~isempty(ret) && isstruct(ret)
                    mousepos_remote = ret;
                end

                % send this computers mouse positions
                [x,y,buttons] = GetMouse(ptb.window);
                if mousepos.x ~= x || mousepos.y ~= y || ~isequal(mousepos.buttons,buttons)
                    mousepos.x = x; mousepos.y = y; mousepos.buttons = buttons;
                    send2buffer(mousepos);
                end

                if mousepos.stagenum == 1
                    % Stage 1: Present the start box
                    Screen('FrameRect', ptb.window, ptb.red, ptb.startPos);
                    % plot own cursor
                    Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        if (~leaderstart && any(buttons) && x>ptb.startPos(1) && x<ptb.startPos(3) && y>ptb.startPos(2) && y<ptb.startPos(4))
                            leaderstart = true;
                        end
                        if (~slavestart && any(mousepos_remote.buttons) && mousepos_remote.x>ptb.startPos(1) && mousepos_remote.x<ptb.startPos(3) ... 
                                && mousepos_remote.y>ptb.startPos(2) && mousepos_remote.y<ptb.startPos(4))
                            slavestart = true;
                        end

                        if leaderstart && slavestart
                            mousepos.stagenum = mousepos.stagenum+1;
                            send2buffer(mousepos); % update the other computer about the next stage
                        end

                    else % if the slave computer
                        if mousepos_remote.stagenum ~= mousepos.stagenum
                            mousepos.stagenum = mousepos_remote.stagenum;
                        end
                    end

                elseif mousepos.stagenum == 2
                    % Stage 2: Present the response boxes
                    Screen('FillRect', ptb.window, ptb.responseColors, ptb.responseRects);
                    % plot own cursor
                    Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        if (y<820 && mousepos_remote.y<820)
                            mousepos.stagenum = mousepos.stagenum+1;
                            send2buffer(mousepos);
                        end

                    else % if the slave computer
                        if mousepos_remote.stagenum ~= mousepos.stagenum
                            mousepos.stagenum = mousepos_remote.stagenum;
                        end
                    end


                elseif mousepos.stagenum == 3
                    % Stage 3: Present cue and record trajectories
                    Screen('FillRect', ptb.window, allColors, allRects);
                    % plot own cursor
                    Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        % if it's blue's turn, we only go to the next trial when that participant responded; same for red
                        if colorNum == 1
                            if any(buttons)
                                if (x>ptb.boxLpos(1) && x<ptb.boxLpos(3) && y>ptb.boxLpos(2) && y<ptb.boxLpos(4))
                                    mousepos.response = 1;
                                    mousepos.stagenum = mousepos.stagenum+1;
                                    send2buffer(mousepos);

                                elseif (x>ptb.boxRpos(1) && x<ptb.boxRpos(3) && y>ptb.boxRpos(2) && y<ptb.boxRpos(4))
                                    mousepos.response = 2;
                                    mousepos.stagenum = mousepos.stagenum+1;
                                    send2buffer(mousepos);
                                end
                            end
                        else % colorNum == 2
                            if any(mousepos_remote.buttons)
                                if (mousepos_remote.x>ptb.boxLpos(1) && mousepos_remote.x<ptb.boxLpos(3) && mousepos_remote.y>ptb.boxLpos(2) && mousepos_remote.y<ptb.boxLpos(4))
                                    mousepos.response = 1;
                                    mousepos.stagenum = mousepos.stagenum+1;
                                    send2buffer(mousepos);

                                elseif (mousepos_remote.x>ptb.boxRpos(1) && mousepos_remote.x<ptb.boxRpos(3) && mousepos_remote.y>ptb.boxRpos(2) && mousepos_remote.y<ptb.boxRpos(4))
                                    mousepos.response = 2;
                                    mousepos.stagenum = mousepos.stagenum+1;
                                    send2buffer(mousepos);
                                end
                            end
                        end 

                    else % if the slave computer
                        if mousepos_remote.stagenum ~= mousepos.stagenum
                            mousepos.stagenum = mousepos_remote.stagenum;
                        end
                    end

                else %mousepos.stagenum == 4    
                    % Present feedback screen
                    trialEnd = GetSecs();
                    trialDuration = num2str(trialEnd - stage1Start,'%.2f');

                    if strcmp(role,leader)
                        if colorNum == mousepos.response
                            responseFeedback = 'CORRECT';
                        else
                            responseFeedback = 'INCORRECT';
                        end

                        feedbackMsg = sprintf(['The response was %s. \n\n', ...
                                               'It took %s seconds to complete the trial.'],...
                                               responseFeedback, trialDuration);

                        Screen('FillRect', ptb.window, ptb.black);
                        DrawFormattedText(ptb.window, feedbackMsg, 'center', 'center', ptb.white);
                        Screen('Flip', ptb.window);

                        mousepos.trialnum = mousepos.trialnum+1;
                        mousepos.stagenum = 1;
                        WaitSecs(2);
                        send2buffer(mousepos);
                        break;

                    else % if the slave computer
                        if colorNum == mousepos_remote.response
                            responseFeedback = 'CORRECT';
                        else
                            responseFeedback = 'INCORRECT';
                        end

                        feedbackMsg = sprintf(['The response was %s. \n\n', ...
                                               'It took %s seconds to complete the trial.'],...
                                               responseFeedback, trialDuration);

                        Screen('FillRect', ptb.window, ptb.black);
                        DrawFormattedText(ptb.window, feedbackMsg, 'center', 'center', ptb.white);
                        Screen('Flip', ptb.window);
                        
                        flag = 0;
                        while flag == 0
                            receive()
                            if ~isempty(ret) && isstruct(ret)
                                mousepos_remote = ret;
                            end

                            if mousepos_remote.trialnum ~= mousepos.trialnum
                                mousepos.trialnum = mousepos_remote.trialnum;
                                flag = 1;
                            end
                        end
                        break;
                    end

                end

            % Go to the next sample
            while GetSecs<nextsampletime
                waitms = max(0,1000*(nextsampletime-GetSecs)/2);
                if waitms > 0.5, java.lang.Thread.sleep(waitms); end
            end
            end
        end
    end

    function practiceRound2(trials)
        mousepos.trialnum = 1;
        [numtr, ~] = size(trials);
        while mousepos.trialnum <= numtr

            Screen('FillRect', ptb.window, ptb.black);

            % Position and color number
            posNum = trials(mousepos.trialnum, 1);
            colorNum = trials(mousepos.trialnum, 2);

            % The position of the cue and the color it is drawn in
            thePos = ptb.cueRects(posNum, :); %1=left, 2=right
            theColor = ptb.cueColors(colorNum, :); %1=blue, 2=red

            % The squares to draw in stage 3
            allRects = [ptb.boxLpos;ptb.boxRpos;thePos]';
            allColors = [ptb.gray; ptb.gray; theColor]';

            % The trajectory times and x,y-coordinates initialized for every trial
            sampleNum = 0;
            mousepos.stagenum = 1;

            % Determine whether a response has been made
            dontsync  = 1; % 1: do not wait for v-sync
            leaderstart = false;
            slavestart = false;

            % Initialize timings
            stage1Start = Screen('Flip', ptb.window);
            nextsampletime = stage1Start;

            stage1Deadline = stage1Start + ptb.deadlines(1) - ptb.halfFlip;

            
            while 1        

                sampleNum = sampleNum + 1;	
                nextsampletime = nextsampletime + ptb.increment;

                % get new remote mouse positions
                receive()
                if ~isempty(ret) && isstruct(ret)
                    mousepos_remote = ret;
                end

                % send this computers mouse positions
                [x,y,buttons] = GetMouse(ptb.window);
                if mousepos.x ~= x || mousepos.y ~= y || ~isequal(mousepos.buttons,buttons)
                    mousepos.x = x; mousepos.y = y; mousepos.buttons = buttons;
                    send2buffer(mousepos);
                end

                if mousepos.stagenum == 1
                    % Stage 1: Present the start box
                    Screen('FrameRect', ptb.window, ptb.red, ptb.startPos);
                    % plot own cursor
                    Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        if (~leaderstart && any(buttons) && x>ptb.startPos(1) && x<ptb.startPos(3) && y>ptb.startPos(2) && y<ptb.startPos(4))
                            leaderstart = true;
                        end
                        if (~slavestart && any(mousepos_remote.buttons) && mousepos_remote.x>ptb.startPos(1) && mousepos_remote.x<ptb.startPos(3) ... 
                                && mousepos_remote.y>ptb.startPos(2) && mousepos_remote.y<ptb.startPos(4))
                            slavestart = true;
                        end

                        if leaderstart && slavestart
                            mousepos.stagenum = mousepos.stagenum+1;
                            send2buffer(mousepos); % update the other computer about the next stage
                            stage2Start = GetSecs();
                            stage2Deadline = stage2Start + ptb.deadlines(2) - ptb.halfFlip;
                        end
                        
                        if GetSecs >= stage1Deadline
                            mousepos.stagenum = 4;
                            send2buffer(mousepos); % update the other computer about the next stage                        
                        end

                    else % if the slave computer
                        if mousepos_remote.stagenum ~= mousepos.stagenum
                            mousepos.stagenum = mousepos_remote.stagenum;
                        end
                    end

                elseif mousepos.stagenum == 2
                    % Stage 2: Present the response boxes
                    Screen('FillRect', ptb.window, ptb.responseColors, ptb.responseRects);
                    % plot own cursor
                    Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        if (y<820 && mousepos_remote.y<820)
                            mousepos.stagenum = mousepos.stagenum+1;
                            send2buffer(mousepos);
                            stage3Start = GetSecs();
                            stage3Deadline = stage3Start + ptb.deadlines(3) - ptb.halfFlip;
                        end
                        
                        if GetSecs >= stage2Deadline
                            mousepos.stagenum = 4;
                            send2buffer(mousepos);
                        end


                    else % if the slave computer
                        if mousepos_remote.stagenum ~= mousepos.stagenum
                            mousepos.stagenum = mousepos_remote.stagenum;
                        end
                    end


                elseif mousepos.stagenum == 3
                    % Stage 3: Present cue and record trajectories
                    Screen('FillRect', ptb.window, allColors, allRects);
                    % plot own cursor
                    Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        % if it's blue's turn, we only go to the next trial when that participant responded; same for red
                        if colorNum == 1
                            if any(buttons)
                                if (x>ptb.boxLpos(1) && x<ptb.boxLpos(3) && y>ptb.boxLpos(2) && y<ptb.boxLpos(4))
                                    mousepos.response = 1;
                                    mousepos.stagenum = 5;
                                    send2buffer(mousepos);

                                elseif (x>ptb.boxRpos(1) && x<ptb.boxRpos(3) && y>ptb.boxRpos(2) && y<ptb.boxRpos(4))
                                    mousepos.response = 2;
                                    mousepos.stagenum = 5;
                                    send2buffer(mousepos);
                                end
                            end
                        else % colorNum == 2
                            if any(mousepos_remote.buttons)
                                if (mousepos_remote.x>ptb.boxLpos(1) && mousepos_remote.x<ptb.boxLpos(3) && mousepos_remote.y>ptb.boxLpos(2) && mousepos_remote.y<ptb.boxLpos(4))
                                    mousepos.response = 1;
                                    mousepos.stagenum = 5;
                                    send2buffer(mousepos);

                                elseif (mousepos_remote.x>ptb.boxRpos(1) && mousepos_remote.x<ptb.boxRpos(3) && mousepos_remote.y>ptb.boxRpos(2) && mousepos_remote.y<ptb.boxRpos(4))
                                    mousepos.response = 2;
                                    mousepos.stagenum = 5;
                                    send2buffer(mousepos);
                                end
                            end
                        end
                        
                        if GetSecs >= stage3Deadline
                            mousepos.stagenum = 4;
                            send2buffer(mousepos);
                        end


                    else % if the slave computer
                        if mousepos_remote.stagenum ~= mousepos.stagenum
                            mousepos.stagenum = mousepos_remote.stagenum;
                        end
                    end

                elseif mousepos.stagenum == 4
                    % Stage 4: Present the warning screen
                    Screen('FillRect', ptb.window, ptb.black);
                    DrawFormattedText(ptb.window, 'Missed deadline!', 'center', 'center', ptb.white);
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        mousepos.trialnum = mousepos.trialnum+1;
                        mousepos.stagenum = 1;
                        WaitSecs(2);
                        send2buffer(mousepos);
                        break;
                    else
                        flag = 0;
                        while flag == 0
                            receive()
                            if ~isempty(ret) && isstruct(ret)
                                mousepos_remote = ret;
                            end

                            if mousepos_remote.trialnum ~= mousepos.trialnum
                                mousepos.trialnum = mousepos_remote.trialnum;
                                flag = 1;
                            end
                        end
                        break;
                    end

                else %mousepos.stagenum == 5    
                    % Present feedback screen
                    trialEnd = GetSecs();
                    trialDuration = num2str(trialEnd - stage1Start,'%.2f');

                    if strcmp(role,leader)
                        if colorNum == mousepos.response
                            responseFeedback = 'CORRECT';
                        else
                            responseFeedback = 'INCORRECT';
                        end

                        feedbackMsg = sprintf(['The response was %s. \n\n', ...
                                               'It took %s seconds to complete the trial.'],...
                                               responseFeedback, trialDuration);

                        Screen('FillRect', ptb.window, ptb.black);
                        DrawFormattedText(ptb.window, feedbackMsg, 'center', 'center', ptb.white);
                        Screen('Flip', ptb.window);

                        mousepos.trialnum = mousepos.trialnum+1;
                        mousepos.stagenum = 1;
                        WaitSecs(2);
                        send2buffer(mousepos);
                        break;

                    else % if the slave computer
                        if colorNum == mousepos_remote.response
                            responseFeedback = 'CORRECT';
                        else
                            responseFeedback = 'INCORRECT';
                        end

                        feedbackMsg = sprintf(['The response was %s. \n\n', ...
                                               'It took %s seconds to complete the trial.'],...
                                               responseFeedback, trialDuration);

                        Screen('FillRect', ptb.window, ptb.black);
                        DrawFormattedText(ptb.window, feedbackMsg, 'center', 'center', ptb.white);
                        Screen('Flip', ptb.window);
                        
                        flag = 0;
                        while flag == 0
                            receive()
                            if ~isempty(ret) && isstruct(ret)
                                mousepos_remote = ret;
                            end

                            if mousepos_remote.trialnum ~= mousepos.trialnum
                                mousepos.trialnum = mousepos_remote.trialnum;
                                flag = 1;
                            end
                        end
                        break;
                    end

                end

                % Go to the next sample
                while GetSecs<nextsampletime
                    waitms = max(0,1000*(nextsampletime-GetSecs)/2);
                    if waitms > 0.5, java.lang.Thread.sleep(waitms); end
                end
            end
        end
    end


    function practiceRound3(trials)
        mousepos.trialnum = 1;
        [numtr, ~] = size(trials);
        while mousepos.trialnum <= numtr

            Screen('FillRect', ptb.window, ptb.black);

            % Position and color number
            posNum = trials(mousepos.trialnum, 1);
            colorNum = trials(mousepos.trialnum, 2);

            % The position of the cue and the color it is drawn in
            thePos = ptb.cueRects(posNum, :); %1=left, 2=right
            theColor = ptb.cueColors(colorNum, :); %1=blue, 2=red

            % The squares to draw in stage 3
            allRects = [ptb.boxLpos;ptb.boxRpos;thePos]';
            allColors = [ptb.gray; ptb.gray; theColor]';

            % The trajectory times and x,y-coordinates initialized for every trial
            sampleNum = 0;
            mousepos.stagenum = 1;

            % Determine whether a response has been made
            dontsync  = 1; % 1: do not wait for v-sync
%             response = -1;
            leaderstart = false;
            slavestart = false;

            % Initialize timings
            stage1Start = Screen('Flip', ptb.window);
%             trialStart = stage1Start;
            stage1Deadline = stage1Start + ptb.deadlines(1) - ptb.halfFlip;
            nextsampletime = stage1Start;

            
            while 1        

                sampleNum = sampleNum + 1;	
                nextsampletime = nextsampletime + ptb.increment;

                % get new remote mouse positions
                receive()
                if ~isempty(ret) && isstruct(ret)
                    mousepos_remote = ret;
                end

                % send this computers mouse positions
                [x,y,buttons] = GetMouse(ptb.window);
                if mousepos.x ~= x || mousepos.y ~= y || ~isequal(mousepos.buttons,buttons)
                    mousepos.x = x; mousepos.y = y; mousepos.buttons = buttons;
                    send2buffer(mousepos);
                end

                if mousepos.stagenum == 1
                    % Stage 1: Present the start box
                    Screen('FrameRect', ptb.window, ptb.red, ptb.startPos);
                    % plot own cursor
                    Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        if (~leaderstart && any(buttons) && x>ptb.startPos(1) && x<ptb.startPos(3) && y>ptb.startPos(2) && y<ptb.startPos(4))
                            leaderstart = true;
                        end
                        if (~slavestart && any(mousepos_remote.buttons) && mousepos_remote.x>ptb.startPos(1) && mousepos_remote.x<ptb.startPos(3) ... 
                                && mousepos_remote.y>ptb.startPos(2) && mousepos_remote.y<ptb.startPos(4))
                            slavestart = true;
                        end

                        if leaderstart && slavestart
                            mousepos.stagenum = mousepos.stagenum+1;
                            send2buffer(mousepos); % update the other computer about the next stage
                            stage2Start = GetSecs();
                            stage2Deadline = stage2Start + ptb.deadlines(2) - ptb.halfFlip;
                        end
                        
                        if GetSecs >= stage1Deadline
                            mousepos.stagenum = 4;
                            send2buffer(mousepos); % update the other computer about the next stage                        
                        end

                    else % if the slave computer
                        if mousepos_remote.stagenum ~= mousepos.stagenum
                            mousepos.stagenum = mousepos_remote.stagenum;
                        end
                    end

                elseif mousepos.stagenum == 2
                    % Stage 2: Present the response boxes
                    Screen('FillRect', ptb.window, ptb.responseColors, ptb.responseRects);
                    % plot own cursor
                    Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        if (y<820 && mousepos_remote.y<820)
                            mousepos.stagenum = mousepos.stagenum+1;
                            send2buffer(mousepos);
                            stage3Start = GetSecs();
                            stage3Deadline = stage3Start + ptb.deadlines(3) - ptb.halfFlip;
                        end
                        
                        if GetSecs >= stage2Deadline
                            mousepos.stagenum = 4;
                            send2buffer(mousepos);
                        end


                    else % if the slave computer
                        if mousepos_remote.stagenum ~= mousepos.stagenum
                            mousepos.stagenum = mousepos_remote.stagenum;
                        end
                    end


                elseif mousepos.stagenum == 3
                    % Stage 3: Present cue and record trajectories
                    Screen('FillRect', ptb.window, allColors, allRects);
                    % plot own cursor
                    Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        % if it's blue's turn, we only go to the next trial when that participant responded; same for red
                        if colorNum == 1
                            if any(buttons)
                                if (x>ptb.boxLpos(1) && x<ptb.boxLpos(3) && y>ptb.boxLpos(2) && y<ptb.boxLpos(4))
                                    mousepos.response = 1;
                                    mousepos.stagenum = 4;
                                    send2buffer(mousepos);

                                elseif (x>ptb.boxRpos(1) && x<ptb.boxRpos(3) && y>ptb.boxRpos(2) && y<ptb.boxRpos(4))
                                    mousepos.response = 2;
                                    mousepos.stagenum = 4;
                                    send2buffer(mousepos);
                                end
                            end
                        else % colorNum == 2
                            if any(mousepos_remote.buttons)
                                if (mousepos_remote.x>ptb.boxLpos(1) && mousepos_remote.x<ptb.boxLpos(3) && mousepos_remote.y>ptb.boxLpos(2) && mousepos_remote.y<ptb.boxLpos(4))
                                    mousepos.response = 1;
                                    mousepos.stagenum = 4;
                                    send2buffer(mousepos);

                                elseif (mousepos_remote.x>ptb.boxRpos(1) && mousepos_remote.x<ptb.boxRpos(3) && mousepos_remote.y>ptb.boxRpos(2) && mousepos_remote.y<ptb.boxRpos(4))
                                    mousepos.response = 2;
                                    mousepos.stagenum = 4;
                                    send2buffer(mousepos);
                                end
                            end
                        end
                        
                        if GetSecs >= stage3Deadline
                            mousepos.stagenum = 4;
                            send2buffer(mousepos);
                        end


                    else % if the slave computer
                        if mousepos_remote.stagenum ~= mousepos.stagenum
                            mousepos.stagenum = mousepos_remote.stagenum;
                        end
                    end

                else %mousepos.stagenum == 4
                    % Stage 4: Present the warning screen
                    Screen('FillRect', ptb.window, ptb.black);
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        mousepos.trialnum = mousepos.trialnum+1;
                        mousepos.stagenum = 1;
                        WaitSecs(1);
                        send2buffer(mousepos);
                        break;
                    else
                        flag = 0;
                        while flag == 0
                            receive()
                            if ~isempty(ret) && isstruct(ret)
                                mousepos_remote = ret;
                            end

                            if mousepos_remote.trialnum ~= mousepos.trialnum
                                mousepos.trialnum = mousepos_remote.trialnum;
                                flag = 1;
                            end
                        end
                        break;
                    end
                end

                % Go to the next sample
                while GetSecs<nextsampletime
                    waitms = max(0,1000*(nextsampletime-GetSecs)/2);
                    if waitms > 0.5, java.lang.Thread.sleep(waitms); end
                end
            end
        end
    end


    function respMat = experiment(trials)
        mousepos.trialnum = 1;
        [numtr, ~] = size(trials);
        respMat(numtr) = struct('block', [], 'cuePos', [], 'cueColor', [],...
            'selectedBox', [], 'startTime', [], 'stimTime', [],...
            'times', [], 'xleader', [], 'yleader', [],'xslave', [], 'yslave', []);

        breakPoint = numtr/8;

        while mousepos.trialnum <= numtr

            Screen('FillRect', ptb.window, ptb.black);

            % Position and color number
            posNum = trials(mousepos.trialnum, 1);
            colorNum = trials(mousepos.trialnum, 2);

            % The position of the cue and the color it is drawn in
            thePos = ptb.cueRects(posNum, :); %1=left, 2=right
            theColor = ptb.cueColors(colorNum, :); %1=blue, 2=red

            % The squares to draw in stage 3
            allRects = [ptb.boxLpos;ptb.boxRpos;thePos]';
            allColors = [ptb.gray; ptb.gray; theColor]';

            % The trajectory times and x,y-coordinates initialized for every trial
            xcoord_leader = respMat(mousepos.trialnum).xleader;
            ycoord_leader = respMat(mousepos.trialnum).yleader;
            xcoord_slave = respMat(mousepos.trialnum).xslave;
            ycoord_slave = respMat(mousepos.trialnum).yslave;
            times = respMat(mousepos.trialnum).times;

            sampleNum = 0;
            mousepos.stagenum = 1;

            % Determine whether a response has been made
            dontsync  = 1; % 1: do not wait for v-sync
%             response = -1;
            leaderstart = false;
            slavestart = false;

            % Initialize timings
            stage1Start = Screen('Flip', ptb.window);
            stage1Deadline = stage1Start + ptb.deadlines(1) - ptb.halfFlip;
            nextsampletime = stage1Start;
            stage2Start = 0;
            stage3Start = 0;

            
            while 1        
                sampleNum = sampleNum + 1;
                times(sampleNum,1) = GetSecs;
                nextsampletime = nextsampletime + ptb.increment;

                % get new remote mouse positions
                receive()
                if ~isempty(ret) && isstruct(ret)
                    mousepos_remote = ret;
                end

                % send this computers mouse positions
                [x,y,buttons] = GetMouse(ptb.window);
                if mousepos.x ~= x || mousepos.y ~= y || ~isequal(mousepos.buttons,buttons)
                    mousepos.x = x; mousepos.y = y; mousepos.buttons = buttons;
                    send2buffer(mousepos);
                end

                xcoord_leader(sampleNum,1) = x;
                ycoord_leader(sampleNum,1) = y;
                xcoord_slave(sampleNum,1) = mousepos_remote.x;
                ycoord_slave(sampleNum,1) = mousepos_remote.y;


                if mousepos.stagenum == 1
                    % Stage 1: Present the start box
                    Screen('FrameRect', ptb.window, ptb.red, ptb.startPos);
                    % plot own cursor
                    Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        if (~leaderstart && any(buttons) && x>ptb.startPos(1) && x<ptb.startPos(3) && y>ptb.startPos(2) && y<ptb.startPos(4))
                            leaderstart = true;
                        end
                        if (~slavestart && any(mousepos_remote.buttons) && mousepos_remote.x>ptb.startPos(1) && mousepos_remote.x<ptb.startPos(3) ... 
                                && mousepos_remote.y>ptb.startPos(2) && mousepos_remote.y<ptb.startPos(4))
                            slavestart = true;
                        end

                        if leaderstart && slavestart
                            mousepos.stagenum = mousepos.stagenum+1;
                            send2buffer(mousepos); % update the other computer about the next stage
                            stage2Start = GetSecs();
                            stage2Deadline = stage2Start + ptb.deadlines(2) - ptb.halfFlip;
                        end
                        
                        if GetSecs >= stage1Deadline
                            mousepos.stagenum = 4;
                            send2buffer(mousepos); % update the other computer about the next stage                        
                        end

                    else % if the slave computer
                        if mousepos_remote.stagenum ~= mousepos.stagenum
                            mousepos.stagenum = mousepos_remote.stagenum;
                        end
                    end

                elseif mousepos.stagenum == 2
                    % Stage 2: Present the response boxes
                    Screen('FillRect', ptb.window, ptb.responseColors, ptb.responseRects);
                    % plot own cursor
                    Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        if (y<820 && mousepos_remote.y<820)
                            mousepos.stagenum = mousepos.stagenum+1;
                            send2buffer(mousepos);
                            stage3Start = GetSecs();
                            stage3Deadline = stage3Start + ptb.deadlines(3) - ptb.halfFlip;
                        end
                        
                        if GetSecs >= stage2Deadline
                            mousepos.stagenum = 4;
                            send2buffer(mousepos);
                        end


                    else % if the slave computer
                        if mousepos_remote.stagenum ~= mousepos.stagenum
                            mousepos.stagenum = mousepos_remote.stagenum;
                        end
                    end


                elseif mousepos.stagenum == 3
                    % Stage 3: Present cue and record trajectories
                    Screen('FillRect', ptb.window, allColors, allRects);
                    % plot own cursor
                    Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        % if it's blue's turn, we only go to the next trial when that participant responded; same for red
                        if colorNum == 1
                            if any(buttons)
                                if (x>ptb.boxLpos(1) && x<ptb.boxLpos(3) && y>ptb.boxLpos(2) && y<ptb.boxLpos(4))
                                    mousepos.response = 1;
                                    mousepos.stagenum = 4;
                                    send2buffer(mousepos);

                                elseif (x>ptb.boxRpos(1) && x<ptb.boxRpos(3) && y>ptb.boxRpos(2) && y<ptb.boxRpos(4))
                                    mousepos.response = 2;
                                    mousepos.stagenum = 4;
                                    send2buffer(mousepos);
                                end
                            end
                        else % colorNum == 2
                            if any(mousepos_remote.buttons)
                                if (mousepos_remote.x>ptb.boxLpos(1) && mousepos_remote.x<ptb.boxLpos(3) && mousepos_remote.y>ptb.boxLpos(2) && mousepos_remote.y<ptb.boxLpos(4))
                                    mousepos.response = 1;
                                    mousepos.stagenum = 4;
                                    send2buffer(mousepos);

                                elseif (mousepos_remote.x>ptb.boxRpos(1) && mousepos_remote.x<ptb.boxRpos(3) && mousepos_remote.y>ptb.boxRpos(2) && mousepos_remote.y<ptb.boxRpos(4))
                                    mousepos.response = 2;
                                    mousepos.stagenum = 4;
                                    send2buffer(mousepos);
                                end
                            end
                        end
                        
                        if GetSecs >= stage3Deadline
                            mousepos.stagenum = 4;
                            send2buffer(mousepos);
                        end


                    else % if the slave computer
                        if mousepos_remote.stagenum ~= mousepos.stagenum
                            mousepos.stagenum = mousepos_remote.stagenum;
                        end
                    end

                else %mousepos.stagenum == 4
                    % Stage 4: Present the warning screen
                    Screen('FillRect', ptb.window, ptb.black);
                    Screen('Flip', ptb.window, 0, 0, dontsync);

                    if strcmp(role,leader)
                        mousepos.trialnum = mousepos.trialnum+1;
                        mousepos.stagenum = 1;
                        WaitSecs(1);
                        send2buffer(mousepos);
                        break;
                    else
                        flag = 0;
                        while flag == 0
                            receive()
                            if ~isempty(ret) && isstruct(ret)
                                mousepos_remote = ret;
                            end

                            if mousepos_remote.trialnum ~= mousepos.trialnum
                                mousepos.trialnum = mousepos_remote.trialnum;
                                flag = 1;
                            end
                        end
                        break;
                    end
 
                end

                % Go to the next sample
                while GetSecs<nextsampletime
                    waitms = max(0,1000*(nextsampletime-GetSecs)/2);
                    if waitms > 0.5, java.lang.Thread.sleep(waitms); end
                end
            end
            %----------------------------------------------------------------------
            %                     Save the data
            %----------------------------------------------------------------------
            if strcmp(role,leader)
                % convert all times to times relative to the start
                times = times(1:sampleNum,:);
                converted_times = times - times(1);

                % save only the points that were sampled
                xcoord_leader = xcoord_leader(1:sampleNum,:);
                ycoord_leader = ycoord_leader(1:sampleNum,:);
                xcoord_slave = xcoord_slave(1:sampleNum,:);
                ycoord_slave = ycoord_slave(1:sampleNum,:);

                % Record the trial data into data matrix
                respMat(mousepos.trialnum-1).block = block;
                respMat(mousepos.trialnum-1).cuePos = posNum;
                respMat(mousepos.trialnum-1).cueColor = colorNum;
                respMat(mousepos.trialnum-1).selectedBox = mousepos.response;
                respMat(mousepos.trialnum-1).startTime = stage2Start - times(1);
                respMat(mousepos.trialnum-1).stimTime = stage3Start - times(1);
                respMat(mousepos.trialnum-1).times = converted_times;
                respMat(mousepos.trialnum-1).xleader = xcoord_leader;
                respMat(mousepos.trialnum-1).yleader = ycoord_leader;
                respMat(mousepos.trialnum-1).xslave = xcoord_slave;
                respMat(mousepos.trialnum-1).yslave = ycoord_slave;
            end
            
            if ismember(mousepos.trialnum-1, [breakPoint, breakPoint*2, breakPoint*3,...
                    breakPoint*5, breakPoint*6, breakPoint*7])
                mousepos.brk = 1;
                while 1
                    % get new remote mouse positions
                    receive()
                    if ~isempty(ret) && isstruct(ret)
                        mousepos_remote = ret;
                    end

                    % send this computers mouse positions
                    send2buffer(mousepos);

                    if mousepos_remote.brk == 1 && mousepos.brk == 1
                        displayBreakMsg(breakMsg);
                        break;
                    end
                end
            elseif mousepos.trialnum-1 == breakPoint*4
                mousepos.brk = 1;
                while 1
                    % get new remote mouse positions
                    receive()
                    if ~isempty(ret) && isstruct(ret)
                        mousepos_remote = ret;
                    end

                    % send this computers mouse positions
                    send2buffer(mousepos);

                    if mousepos_remote.brk == 1 && mousepos.brk == 1
                        displayBreakMsg(halfMsg);
                        break;
                    end
                end
            end

        end
    end


    function displayBreakMsg(msg)
        % Present a message screen and wait for clicking on the "ready" button

        while 1
            % get new remote mouse positions
            receive()
            if ~isempty(ret) && isstruct(ret)
                mousepos_remote = ret;
            end

            % send this computers mouse positions
            [x,y,buttons] = GetMouse(ptb.window);
            if mousepos.x ~= x || mousepos.y ~= y || ~isequal(mousepos.buttons,buttons)
                mousepos.x = x; mousepos.y = y; mousepos.buttons = buttons;
                send2buffer(mousepos);
            end
            
            Screen('FillRect', ptb.window, ptb.gray, ptb.startPos);
            DrawFormattedText(ptb.window, msg, 150, 50, ptb.white, [], [], [], 1.25);
            Screen('DrawTexture', ptb.window, texel1, [], CenterRectOnPoint(spriteRect, x, y));
            Screen('Flip', ptb.window);

            if strcmp(role,leader)
                if (~leaderdone && any(buttons) && x>ptb.startPos(1) && x<ptb.startPos(3) ...
                        && y>ptb.startPos(2) && y<ptb.startPos(4))
                    leaderdone = true;
                end
                if (~slavedone && any(mousepos_remote.buttons) && mousepos_remote.x>ptb.startPos(1) ...
                        && mousepos_remote.x<ptb.startPos(3) && mousepos_remote.y>ptb.startPos(2) ...
                        && mousepos_remote.y<ptb.startPos(4))
                    slavedone = true;
                end

                % If both clicked on the ready button, go on
                if leaderdone && slavedone
                    while any(buttons) || any(mousepos_remote.buttons)
                        % get new remote mouse positions
                        receive()
                        if ~isempty(ret) && isstruct(ret)
                            mousepos_remote = ret;
                        end
                        [~,~,buttons] = GetMouse(ptb.window);
                    end

                    leaderdone = false;
                    slavedone = false;
                    mousepos.brk = 0;
                    send2buffer(mousepos);
                    break;
                end

            else
                if mousepos_remote.brk == 0
                    mousepos.brk = 0;
                    break;
                end
            end
        end
    end


    function send2buffer(mp) % mp: mousepos
        %channels: role (server=1,client=2), mouse x-position, mouse y-position, trial number, 
        % stage number, phase number, response, button1, button2, button?
        mousedata = [rolenumber; mp.x; mp.y; mp.trialnum; mp.stagenum; mp.phasenum; mp.response; mp.brk; mp.buttons(:)];
        sndClient.putData(mousedata(:),[size(mousedata,2) size(mousedata,1)]);
    end

    function receive
        ret = []; % init
        hdr = rcvClient.getHeader;
        % check new events (Note: events not used)
        if hdr.nEvents > nEvents
            % get the last event for this process
            evts = rcvClient.getEvents(nEvents,hdr.nEvents-1); % only pick last one
            for n = numel(evts) : -1: 1
                if ~strcmp(char(evts(n).getType),role)
                    ret=evts(n).getValue.getArray; % Note: 1-based counting here!!
                    break;
                end
            end
            nEvents = hdr.nEvents;
        end
        % check new incoming data
        if hdr.nSamples > nSamples
            % get last data sample
            % only pick the last data sample from the remote buffer, it holds the most recent content/info
            data = rcvClient.getDoubleData(hdr.nSamples-1,hdr.nSamples-1);
            if ~isempty(data)
                ret = struct('role',roles{data(end,ROLE)}, ...
                    'x',data(end,XPOS),'y',data(end,YPOS), ...
                    'trialnum',data(end,TRIAL),'stagenum',data(end,STAGE), ...
                    'phasenum',data(end,PHASE),'response',data(end,RESPONSE),...
                    'brk',data(end,BRK),'buttons',logical(data(end,BRK+1:end)));
                % disp(ret);
            end
            nSamples = hdr.nSamples;
        end
    end

    function install_fieldtrip_server()
        % Initiate FieldTrip buffer server
        bufferServer.BufferServerStart(svport,120000,500);
        % connect client to this FieldTrip buffer to receive remote mouse info
        sndClient.connect('localhost',svport);
        % Start buffer with correct header, determine number of required channels
        [~,~,buttons] = GetMouse(ptb.window);
        % 1=role,3=xpos,4=ypos,5=trial,6=stage,7=phase,8=response,9=brk, 10=btn1,11=btn2,12=btn3,....
        numChannels = BRK + numel(buttons); 
        Fs = 1; % not meaningful (intermittent data, depends on mouse movements)
        dataType = 10; % FLOAT32
        import('nl.fcdonders.fieldtrip.bufferclient.Header');
        hdr=Header(numChannels,Fs,dataType);
        sndClient.putHeader(hdr);
    end

    function connect_client()
        % Connect client to remote FieldTrip buffer for sending this mouse info to remote computer
        fprintf('Waiting for FieldTrip buffer...');
        while 1
            try
                rcvClient.connect(ip,clport);
            catch
                fprintf('.');
                pause(0.5);
                continue
            end
            fprintf('\n');
            break % we're connected
        end
        while 1
            try
                rcvClient.getHeader();
            catch
                fprintf(':');
                pause(0.5);
                continue
            end
            fprintf('\n');
            break % header defined
        end
    end

    function cleanup()
        try closePTB(); catch, end
        try psychrethrow(psychlasterror); catch, end
        stop_buffer();
    end

    function stop_buffer()
        if exist('bufferServer','var') && ~isempty(bufferServer)
            try bufferServer.stopBuffer(); catch, end
            bufferServer = [];
        end
        % get object to access static list of running fieldtrip servers
        import nl.fcdonders.fieldtrip.bufferserver.BufferServer
        srv = BufferServer;
        srv.removeAllServerSockets;
    end


end