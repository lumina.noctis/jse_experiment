function convertData(nSub)

data_path = [pwd '/Data/Experiment2'];
addpath(data_path);

results_path = [pwd '/Results/'];

for i=1:nSub
    data2csv(i);
end


function data2csv(subjNum)

resultsFile = sprintf('Responses_%i.mat', subjNum);
respMat = importdata(resultsFile);

[~, ncols] = size(respMat);

for col = 1:ncols
    respMat(col).trial = col;
    respMat(col).subid = sprintf('%03d', subjNum);
    respMat(col).x = respMat(col).x(:,2);
    respMat(col).y = respMat(col).y(:,2);
end

temp_table = struct2table(respMat);
temp_table = temp_table(:, [1:3 end-1:end 4:end-2]);

csvname = sprintf(strcat(results_path, 's%s.csv'), respMat(1).subid);
writetable(temp_table, csvname);

end

end