function practiceRound1(trialMatrix, ptb, block)
	% Show the hand-cursor in the bottom center of the screen
	imgLocation = 'hand3.png';
    [image, ~, alpha] = imread(imgLocation);
    image(:,:,4) = alpha(:,:);
    texel = Screen('MakeTexture', ptb.window, image);
    [s1,s2,~] = size(image);
    imgH = s1 * 0.05;
    imgW = s2 * 0.05;
    spriteRect = [0 0 imgW imgH]; % The bounding box for our animated sprite

    trialNum = 1;
    [numTrials, ~] = size(trialMatrix);

    while trialNum <= numTrials
        Screen('FillRect', ptb.window, ptb.black);
		%------------------------------------------------------------------
		%                   Initialize all variables
		%------------------------------------------------------------------
		% Position and color number from the shuffled matrix
		posNum = trialMatrix(trialNum, 1);
		colorNum = trialMatrix(trialNum, 2);

		% The position of the cue and the color it is drawn in
		thePos = ptb.cueRects(posNum, :);
		theColor = ptb.cueColors(colorNum, :);

        % The squares to draw in stage 3
        allRects = [ptb.boxLpos; ptb.boxRpos; thePos]';
        allColors = [ptb.gray; ptb.gray; theColor]';

		% Initialize the trajectory times, x,y-coordinates and sample number
		sampleNum = 0;
        stageNum = 1;
        
        % Determine whether a response has been made
        response = -1;
        dontsync  = 1; % do not wait for v-sync      
        start = false;

        % Initialize timings
        stage1Start = Screen('Flip', ptb.window);
        trialStart = stage1Start;
        stage1Deadline = stage1Start + ptb.round1Deadlines(1) - ptb.halfFlip;
        %begintime = GetSecs; -> same as stage1Start
        nextsampletime = stage1Start;


        while 1
            
            sampleNum = sampleNum + 1;
            nextsampletime = nextsampletime + ptb.increment;

            % get new mouse positions
            [x,y,buttons] = GetMouse(ptb.window);

            if stageNum == 1
                %------------------------------------------------------------------
                %                   Stage 1: Present the start box
                %------------------------------------------------------------------
                % Draw the start square. The start square stays on the screen
                % until the participant clicks it or 1,5 seconds pass.
                % If any of the deadlines is missed, the next trial starts.

                Screen('FrameRect', ptb.window, ptb.gray, ptb.startPos);
                % plot remote cursor
                Screen('DrawTexture', ptb.window, texel, [], CenterRectOnPoint(spriteRect, x, y));
                %stage1Start = Screen('Flip', ptb.window, 0, 0, dontsync);
                Screen('Flip', ptb.window, 0, 0, dontsync);
 
                if (~start && any(buttons) && x>ptb.startPos(1) && x<ptb.startPos(3) && y>ptb.startPos(2) && y<ptb.startPos(4))
                    start = true;
                end

                if start
                    stageNum = stageNum+1;
                    stage2Start = GetSecs();
                    stage2Deadline = stage2Start + ptb.round1Deadlines(2) - ptb.halfFlip;
                end

                if GetSecs >= stage1Deadline
                    stageNum = 4;
                end

                
            elseif stageNum == 2
                %------------------------------------------------------------------
                %                   Stage 2: Present the response boxes
                %------------------------------------------------------------------
                % Once they pressed the start button and released it, show the response boxes
                % Wait for both participant to start moving by checking whether they have 
                % moved by at least 5 pixels in the upward direction counting from the
                % boundary of the start button, i.e. if their position is above (lower than)
                % the coordinate y=820.

                Screen('FillRect', ptb.window, ptb.responseColors, ptb.responseRects);
                Screen('DrawTexture', ptb.window, texel, [], CenterRectOnPoint(spriteRect, x, y));
                Screen('Flip', ptb.window, 0, 0, dontsync);

                if (y<820)
                    stageNum = stageNum+1;
                    stage3Start = GetSecs();
                    stage3Deadline = stage3Start + ptb.round1Deadlines(3) - ptb.halfFlip;
                end

                if GetSecs >= stage2Deadline
                    stageNum = 4;
                end

            elseif stageNum == 3
                %------------------------------------------------------------------
                %                   Stage 3: Present cue and record trajectories
                %------------------------------------------------------------------
                % One person was asked to respond to the blue cue by moving to the left RB and clicking, 
                % the other - to the red cue by moving to the right RB and clicking.
                % Draw the cue and show it after random delay.
                % The trial ends after clicking on one of the response boxes with the deadline of 2s.

                Screen('FillRect', ptb.window, allColors, allRects);
                Screen('DrawTexture', ptb.window, texel, [], CenterRectOnPoint(spriteRect, x, y));
                Screen('Flip', ptb.window, 0, 0, dontsync);

                if colorNum == 1
                    if any(buttons)
                        if (x>ptb.boxLpos(1) && x<ptb.boxLpos(3) && y>ptb.boxLpos(2) && y<ptb.boxLpos(4))
                            response = 1;
                            stageNum = 5;
                        elseif (x>ptb.boxRpos(1) && x<ptb.boxRpos(3) && y>ptb.boxRpos(2) && y<ptb.boxRpos(4))
                            response = 2;
                            stageNum = 5;
                        end
                    end
                else % colorNum == 2
                    if any(buttons)
                        if (x>ptb.boxLpos(1) && x<ptb.boxLpos(3) && y>ptb.boxLpos(2) && y<ptb.boxLpos(4))
                            response = 1;
                            stageNum = 5;
                        elseif (x>ptb.boxRpos(1) && x<ptb.boxRpos(3) && y>ptb.boxRpos(2) && y<ptb.boxRpos(4))
                            response = 2;
                            stageNum = 5;
                        end
                    end
                end 

                if GetSecs >= stage3Deadline
                    if block == 1 || block == 3
                        if colorNum == 1
                            stageNum = 4;
                        else
                            stageNum = 5;
                        end
                    elseif block == 2 || block == 4
                        if colorNum == 2
                            stageNum = 4;
                        else
                            stageNum = 5;
                        end                        
                    end
                end
                
            elseif stageNum == 4
                %------------------------------------------------------------------
                %                   Stage 4: Present the warning screen
                %------------------------------------------------------------------
                % The whole screen is colored red if any of the time deadlines
                % is missed. At present it also shows in between trials.

                Screen('FillRect', ptb.window, ptb.black);
                DrawFormattedText(ptb.window, 'Missed deadline!', 'center', 'center', ptb.white);
                Screen('Flip', ptb.window, 0, 0, dontsync);
                WaitSecs(2);
                break;
                
            else %stageNum == 5
                % Present feedback screen
                trialEnd = GetSecs();
                trialDuration = num2str(trialEnd - trialStart,'%.2f');

                if block == 1 || block == 3
                    if colorNum == 1
                        if colorNum == response
                            responseFeedback = 'CORRECT';
                        else
                            responseFeedback = 'INCORRECT';
                        end
                    else
                        if response < 0
                            responseFeedback = 'CORRECT';
                        else
                            responseFeedback = 'INCORRECT';
                        end
                    end
                elseif block == 2 || block == 4
                    if colorNum == 2                         
                        if colorNum == response
                            responseFeedback = 'CORRECT';
                        else
                            responseFeedback = 'INCORRECT';
                        end
                    else
                        if response < 0
                            responseFeedback = 'CORRECT';
                        else
                            responseFeedback = 'INCORRECT';
                        end
                    end
                end
                
                feedbackMsg = sprintf(['Your response was %s. \n\n', ...
                                       'You took %s seconds to complete the trial.'],...
                                       responseFeedback, trialDuration);
                
                Screen('FillRect', ptb.window, ptb.black);
                DrawFormattedText(ptb.window, feedbackMsg, 'center', 'center', ptb.white);
                Screen('Flip', ptb.window);
                WaitSecs(2);
                break;

            end

            while GetSecs<nextsampletime % this timing is more precise than WaitSecs
            end   

        end
        trialNum = trialNum + 1;
    end

end