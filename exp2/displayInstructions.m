function displayInstructions(block, ptb)

    if block == 1
        startInfo1 = fileread('simon_instructions_b1.txt');
    elseif block == 2
        startInfo1 = fileread('simon_instructions_b2.txt');
    elseif block == 3
        startInfo1 = fileread('simon_instructions_b3.txt');
    elseif block == 4
        startInfo1 = fileread('simon_instructions_b4.txt');
    else
        error('Block should be either 1 or 2');
    end

    startInfo2 = fileread('simon_instructions2.txt');
    textpage = 1;

    while 1
      if textpage == 1
        % Present a start screen and wait for a key-press
        DrawFormattedText(ptb.window, startInfo1, 150, 50, ptb.white, [], [], [], 1.25);
        Screen('Flip', ptb.window);
      
        [~, keyCode] = KbWait;
        if keyCode(ptb.rightarrow)
          textpage = 2;
        end
      end

      if textpage == 2
        % Present a start screen and wait for a key-press
        DrawFormattedText(ptb.window, startInfo2, 150, 50, ptb.white, [], [], [], 1.25);
        Screen('Flip', ptb.window);
        
        [~, keyCode] = KbWait;
        if keyCode(ptb.leftarrow)
          textpage = 1;
        elseif keyCode(ptb.space)
          break;
        end
      end
    
    end

end