function condMeans = transformPoints(respMat)
width = 1920;
height = 1080;
numTrials = length(respMat);
respMatNorm = respMat;

% normalize space
for i=1:numTrials
    points = respMat(i).coordinates;
    respMatNorm(i).xSpNorm = points(:,1)-width/2;
    respMatNorm(i).ySpNorm = height - points(:,2);
end

% normalize time
for i=1:numTrials
    % create 101 time intervals
    step = (max(respMat(i).times)-min(respMat(i).times))/100;
    nt = min(respMat(i).times):step:max(respMat(i).times);
    % interpolate coordinates using the time intervals
    nx = interp1q(respMatNorm(i).times, respMatNorm(i).xSpNorm, nt');
    ny = interp1q(respMatNorm(i).times, respMatNorm(i).ySpNorm, nt');
    respMatNorm(i).xTimeNorm = nx;
    respMatNorm(i).yTimeNorm = ny;
end

mx11 = [];
mx12 = [];
mx21 = [];
mx22 = [];

my11 = [];
my12 = [];
my21 = [];
my22 = [];

for i=1:numTrials
    if respMatNorm(i).cueColor == 1
        if respMatNorm(i).cuePos == 1
            mx11(:,end+1) = respMatNorm(i).xTimeNorm;
            my11(:,end+1) = respMatNorm(i).yTimeNorm;
        else
            mx12(:,end+1) = respMatNorm(i).xTimeNorm;
            my12(:,end+1) = respMatNorm(i).yTimeNorm;
        end
    else
        if respMatNorm(i).cuePos == 2
            mx21(:,end+1) = respMatNorm(i).xTimeNorm;
            my21(:,end+1) = respMatNorm(i).yTimeNorm;
        else
            mx22(:,end+1) = respMatNorm(i).xTimeNorm;
            my22(:,end+1) = respMatNorm(i).yTimeNorm;
        end
    end
end

condMeans = [mean(mx11,2),mean(mx12,2),mean(mx21,2),mean(mx22,2),mean(my11,2),mean(my12,2),mean(my21,2),mean(my22,2)];

% figure(1)
% hold on
% plot(condMeans(:,1), condMeans(:,5), '-b');
% plot(condMeans(:,2), condMeans(:,6), ':b');
% plot(condMeans(:,3), condMeans(:,7), '-r');
% plot(condMeans(:,4), condMeans(:,8), ':r');
% xlim([-width/2,width/2]);
% ylim([0,height]);



% figure(2)
% hold on
% for i=1:numTrials
%     if respMatNorm(i).cueColor == 1
%         if respMatNorm(i).cuePos == 1
%             plot(respMatNorm(i).xTimeNorm, respMatNorm(i).yTimeNorm, '-b');
%         else
%             plot(respMatNorm(i).xTimeNorm, respMatNorm(i).yTimeNorm, ':b');
%         end
%     else
%         if respMatNorm(i).cuePos == 2
%             plot(respMatNorm(i).xTimeNorm, respMatNorm(i).yTimeNorm, '-r');
%         else
%             plot(respMatNorm(i).xTimeNorm, respMatNorm(i).yTimeNorm, ':r');
%         end
%     end
% end
% xlim([-width/2,width/2]);
% ylim([0,height]);

end