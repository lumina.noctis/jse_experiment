% Initialize folder paths
addpath(genpath('/Applications/Psychtoolbox/'));
cd('/Users/katja/matlabwd/KatjaCCS2')

% Set data folder
data_path = [pwd '/Data/'];
addpath(data_path);

% Set utils folder
utils_path = [pwd '/utils/'];
addpath(utils_path);


% Initialize file names
blockOrderFile = strcat(data_path, 'blockOrderExp2.mat');

% Seed the random number generator
rng('default');
rng(32);

blocks = 1:4;
numParts = 20;
repeats = numParts/length(blocks);
blockList = repmat(blocks, 1, repeats);
blockOrder = Shuffle(blockList);

% Save
save(blockOrderFile, 'blockOrder');