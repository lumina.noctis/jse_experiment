% Initialize folder paths
addpath(genpath('/Applications/Psychtoolbox/'));
cd('/Users/katja/matlabwd/KatjaCCS2')

% Set data folder
data_path = [pwd '/Data/'];
addpath(data_path);

% Set utils folder
utils_path = [pwd '/utils/'];
addpath(utils_path);


% Initialize file names
trialorderFileDemo = strcat(data_path, 'trialorderExp2Demo.mat');
practiceorderFileDemo = strcat(data_path, 'practiceorderExp2Demo.mat');
trialorderFile = strcat(data_path, 'trialorderExp2.mat');
practiceorderFile = strcat(data_path, 'practiceorderExp2.mat');

% Seed the random number generator
rng('default');
rng(13);

conditions = [1, 1; 1, 2; 2, 1; 2, 2];

numRepetitionsDemo = 1;
numRepetitions = 10;

numRepetitionsPracticeDemo = 3;
numRepetitionsPractice = 10;

numParticipantsPractice = 1; % the same practice matrix given to all participants
numParticipants = 20;

constraintPractice = 'single trial';
constraintTest = 'two trials';

% Generate trial matrices
practiceMatrixDemo = randomizeTrials(conditions, numRepetitionsPracticeDemo, numParticipantsPractice, constraintPractice);
trialMatrixDemo = randomizeTrials(conditions, numRepetitionsDemo, numParticipants, constraintTest);
practiceMatrix = randomizeTrials(conditions, numRepetitionsPractice, numParticipantsPractice, constraintPractice);
% Create two separate blocks
trialMatrix1 = randomizeTrials(conditions, numRepetitions, numParticipants, constraintTest);
trialMatrix2 = randomizeTrials(conditions, numRepetitions, numParticipants, constraintTest);
% Combine blocks
trialMatrix = cell(1, numParticipants);
for i=1:numParticipants
    trialMatrix{1,i} = [trialMatrix1{1,i}; trialMatrix2{1,i}];
end

% Save all files
save(trialorderFileDemo, 'trialMatrixDemo');
save(practiceorderFileDemo, 'practiceMatrixDemo');
save(trialorderFile, 'trialMatrix');
save(practiceorderFile, 'practiceMatrix');