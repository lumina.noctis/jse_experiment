function trialMatrix = randomizeTrials(conditions, numRepetitions, numParticipants, constraint)

% Script to create a randomized trial matrix for a complete experiment.
%
% INPUT:
% - conditions (matrix): required conditions
% - numRepetitions (int): required number of repetitions for each unique condition
% - numParticipants (int): required number of participants
% - constraint (str): 'single trial' or 'two trials', required type of constraint
%
% OUTPUT:
% Returns a cell trialMatrix containing numParticipants matrices, where each 
% matrix contains the settings for each trial. Each row represents a trial 
% and each column represents a variable.
%
% Example use
% Conditions: 2x2=4 cue color and position combinations
% conditions = [1, 1; 1, 2; 2, 1; 2, 2];
% trialMatrix = randomizeTrials(conditions, 5, 20, 'single trial')


trialMatrix = cell(1, numParticipants);

if strcmp(constraint, 'single trial') % this is used to create practice trials only
    for numParticipant=1:numParticipants
        % Duplicate the condition matrix to get the full number of trials
        trialMatrixBase = repmat(conditions, numRepetitions, 1);
        % Get the size of the matrix
        [numTrials, ~] = size(trialMatrixBase);
        % split in 3 rounds
        round1 = trialMatrixBase(1:numTrials/4,:);
        round2 = trialMatrixBase(numTrials/4+1:numTrials/2,:);
        round3 = trialMatrixBase(numTrials/2+1:numTrials,:);
        % randomize order within each round
        shuffler1 = Shuffle(1:numTrials/4);
        shuffler2 = Shuffle(1:numTrials/4);
        shuffler3 = Shuffle(1:numTrials/2);
        
        round1trials = round1(shuffler1,:);
        round2trials = round2(shuffler2,:);
        round3trials = round3(shuffler3,:);
        
        trialMatrix{1, numParticipant} = vertcat(round1trials, round2trials, round3trials);
    end
elseif strcmp(constraint, 'two trials')
    for numParticipant=1:numParticipants
        a = conditions;
        b = conditions;
        [numRows, numCols] = size(conditions);
        % Combine conditions in two consecutive trials to get all the combinations
        m1 = zeros(numRows^2, numCols^2);
        for i=1:4
            row = 4*(i-1);
            for j=1:4
                m1(row+j,1:2) = a(i,:);
                m1(row+j,3:4) = b(j,:);
            end
        end
        % Duplicate the condition matrix to get the full number of trials
        m2 = repmat(m1, numRepetitions, 1);
        % Get the size of the trial matrix
        [halfNumTrials, ~] = size(m2);
        numTrials = halfNumTrials * 2;
        
        conditionMet = false;
        
        while ~conditionMet
            conditionMet = true;
            disp('shuffling...')
            % Randomize the conditions
            shuffler = Shuffle(1:halfNumTrials);
            m3 = m2(shuffler, :);
            % Unpack the conditions into consecutive trials following each other
            m4 = zeros(numTrials, 2);
            for i = 1:halfNumTrials
                m4(i+(i-1),:) = m3(i,1:2);
                m4(i+i,:) = m3(i,3:4);
            end

            count11 = zeros(1,4);
            count12 = zeros(1,4);
            count21 = zeros(1,4);
            count22 = zeros(1,4);

            m5 = [];
            m5(1,:) = m4(1,:);
            m4(1,:) = [];
            i = 2;
            tic
            while length(m5) < numTrials
                if toc > 5
                    conditionMet = false;
                    break;
                end
                m5(i,:) = m4(1,:);
                prev = m5(i-1,:);
                curr = m5(i,:);
                if isequal(prev, [1,1])
                    if isequal(curr,[1,1])
                        count11(1,1) = count11(1,1) + 1;
                        if count11(1,1) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end
                    elseif isequal(curr,[1,2])
                        count11(1,2) = count11(1,2) + 1;
                        if count11(1,2) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    elseif isequal(curr,[2,1])
                        count11(1,3) = count11(1,3) + 1;
                        if count11(1,3) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    elseif isequal(curr,[2,2])
                        count11(1,4) = count11(1,4) + 1;
                        if count11(1,4) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    end
                elseif isequal(prev, [1,2])
                    if isequal(curr,[1,1])
                        count12(1,1) = count12(1,1) + 1;
                        if count12(1,1) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    elseif isequal(curr,[1,2])
                        count12(1,2) = count12(1,2) + 1;
                        if count12(1,2) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    elseif isequal(curr,[2,1])
                        count12(1,3) = count12(1,3) + 1;
                        if count12(1,3) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    elseif isequal(curr,[2,2])
                        count12(1,4) = count12(1,4) + 1;
                        if count12(1,4) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    end
                elseif isequal(prev, [2,1])
                    if isequal(curr,[1,1])
                        count21(1,1) = count21(1,1) + 1;
                        if count21(1,1) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    elseif isequal(curr,[1,2])
                        count21(1,2) = count21(1,2) + 1;
                        if count21(1,2) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    elseif isequal(curr,[2,1])
                        count21(1,3) = count21(1,3) + 1;
                        if count21(1,3) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    elseif isequal(curr,[2,2])
                        count21(1,4) = count21(1,4) + 1;
                        if count21(1,4) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    end
                elseif isequal(prev, [2,2]) 
                    if isequal(curr,[1,1])
                        count22(1,1) = count22(1,1) + 1;
                        if count22(1,1) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    elseif isequal(curr,[1,2])
                        count22(1,2) = count22(1,2) + 1;
                        if count22(1,2) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    elseif isequal(curr,[2,1])
                        count22(1,3) = count22(1,3) + 1;
                        if count22(1,3) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    elseif isequal(curr,[2,2])
                        count22(1,4) = count22(1,4) + 1;
                        if count22(1,4) > 2*numRepetitions
                            m4(end+1,:) = m4(1,:);
                            m4(1,:) = [];
                            continue;
                        end

                    end
                end
                i = i+1;
                m4(1,:) = [];
            end
            if ~conditionMet
                continue;
            end
                
            
        end
        % Record the trial order
        trialMatrix{1, numParticipant} = m5;
    end
else
    disp('Invalid constraint');
end

%         constraintMet = false;
%         while ~constraintMet
%             % Randomize the conditions
%             shuffler = Shuffle(1:halfNumTrials);
%             m3 = m2(shuffler, :);
%             % Unpack the conditions into consecutive trials following each other
%             m4 = zeros(numTrials, 2);
%             for i = 1:halfNumTrials
%                 m4(i+(i-1),:) = m3(i,1:2);
%                 m4(i+i,:) = m3(i,3:4);
%             end
%             allcounts = checkConstraints(m4);
%             if sum(sum(allcounts <= 20)) == 16
%                 constraintMet = true;
%             end
%         end


end