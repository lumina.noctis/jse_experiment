function masterscript(mode, subjectNum)
% This is a masterscript for the Mouse-tracking Social Simon task experiment.
% It initializes the experiment and saves the data.
%
% This is a masterscript for the Mouse-tracking GoNogo Simon task experiment.
% It runs the whole experiment and saves the data.
%
% mode (str): experimental mode, 'demo' or 'test'
% block (int): block, 1 = blue left, red nothing
%                     2 = blue right, red nothing
%                     3 = red left, blue nothing
%                     4 = red right, blue nothing
% subjectNum (int): the participant number used for data storage;

addpath(genpath('/Applications/Psychtoolbox/'));
%cd('/Users/katja/matlabwd/twomice')

% set data folder:
data_path = [pwd '/Data/'];
addpath(data_path);

%----------------------------------------------------------------------
%                     Psychtoolbox housekeeping
%----------------------------------------------------------------------
% get old Psychtoolbox preferences
oldVisualDebugLevel = Screen('Preference', 'VisualDebugLevel');
oldSupressAllWarnings = Screen('Preference', 'SuppressAllWarnings');
oldSyncTests = Screen('Preference', 'SkipSyncTests');

% set new preferences
Screen('Preference', 'VisualDebugLevel', 3);
Screen('Preference', 'SuppressAllWarnings', 1);
Screen('Preference', 'SkipSyncTests', 1);

practiceorderFileDemo = strcat(data_path, 'practiceorderExp2Demo.mat');
trialorderFileDemo = strcat(data_path, 'trialorderExp2Demo.mat');
practiceorderFile = strcat(data_path, 'practiceorderExp2.mat');
trialorderFile = strcat(data_path, 'trialorderExp2.mat');

practiceTrialMatrixDemo = importdata(practiceorderFileDemo);
trialMatrixDemo = importdata(trialorderFileDemo);

practiceTrialMatrix = importdata(practiceorderFile);
trialMatrix = importdata(trialorderFile);

cond_path = strcat(data_path, 'Experiment2/');

blockOrderFile = strcat(data_path, 'blockOrderExp2.mat');
blockOrder = importdata(blockOrderFile);

block = blockOrder(subjectNum);

% initialize Psychtoolbox
ptb = initPTB_simon(block);



% Display instructions
displayInstructions(block, ptb);
KbWait([],1);
if strcmp(mode, 'demo')
    % Run practice rounds
    runPractice(practiceTrialMatrixDemo{1,1}, ptb, block);
    interInfo = (['You have completed the practice rounds.\n\n',...
        'Press space to begin the experiment']);
    displayMsg(interInfo, ptb);
    % Run experiment
    respMat = exp2(trialMatrixDemo{subjectNum}, ptb, block); %#ok
    % Save data
    responsesFile = sprintf('DemoResponses_%i.mat', subjectNum);
    save(strcat(cond_path, responsesFile), 'respMat');
elseif strcmp(mode, 'test')
    % Run practice rounds
    runPractice(practiceTrialMatrix{1,1}, ptb, block);
    interInfo = (['You have completed the practice rounds.\n\n',...
        'Press space to begin the experiment']);
    displayMsg(interInfo, ptb);
    % Run experiment
    respMat = exp2(trialMatrix{subjectNum}, ptb, block); %#ok
    % Save data
    responsesFile = sprintf('Responses_%i.mat', subjectNum);
    save(strcat(cond_path, responsesFile), 'respMat');
end

endInfo = 'Experiment finished. \n\n Press space to exit.';
displayMsg(endInfo, ptb);

cleanup();
% [~, keyCode] = KbWait;
% if keyCode(ptb.space)
%     cleanup();
% end

function cleanup()
    try closePTB(); catch, end
    try psychrethrow(psychlasterror); catch, end
end

%----------------------------------------------------------------------
%                     Psychtoolbox housekeeping
%----------------------------------------------------------------------
% Restore preferences
Screen('Preference', 'VisualDebugLevel', oldVisualDebugLevel);
Screen('Preference', 'SuppressAllWarnings', oldSupressAllWarnings);
Screen('Preference', 'SkipSyncTests', oldSyncTests);

end