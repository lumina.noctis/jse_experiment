In this experiment, you are requested to respond to one of the two different 
colors by moving the mouse to one of the response boxes that will appear in 
the top corners of the screen.

At the start of every trial you will see a small red rectangular frame at 
the bottom of the screen. We call it a "start button". You need to click on it 
to begin the trial. Please make sure that when you click, the cursor is completely 
inside the start button. If you do not click the start button within 1.5 seconds, 
the trial ends. You will see the screen turn black briefly and a new trial 
will begin.

If you do click the start button within the required time, you will see two 
gray response boxes appear at the top of the screen. You are required to start 
moving upwards as soon as you see the response boxes. You will have 1.5 seconds 
to start moving. Again, if you do not meet the deadline, you will see a black 
screen and a new trial will begin.

If you do start moving as requested, a color cue will appear on the screen,
on the left or on the right side. If the cue that appears is red, you should 
move to the left response box and click on it, irrespective of where the cue 
appears. If the cue that appears is blue, you should do nothing. You will have 
2 seconds to complete this part of the trial.

Press the right arrow to go to the next page.
