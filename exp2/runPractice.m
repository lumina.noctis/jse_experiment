function runPractice(practiceTrialMatrix, ptb, block)
    
% This is a script for running practice rounds of the mouse-tracking individual 
% Simon task version.
% It presents 3 rounds in which the participant is required to respond to the
% blue cue by moving to the left response box and to the red cue by moving to 
% the right response box. 
%
% Round 1: 10 trials, does not include time deadlines, provides feedback 
%           on whether the response was correct and how long the participant took.
% Round 2: 10 trials, includes time deadlines, provides feedback as in R1.
% Round 3: 20 trials, includes time deadlines, no feedback.
  
    round1Msg = ['Practice Round 1 involves long time deadlines.\n\n',...
            'After every trial you will be given feedback on whether your response\n',...
            'was correct and how much time you took to complete the trial\n\n',...
            'Press space to begin the Practice Round 1.'];
    round2Msg = ['Practice Round 2 involves actual time deadlines.\n\n',...
            'You have 1.5 seconds to click on the start button at the bottom of the screen,\n',...
            '1.5 seconds to start moving upwards and 2 seconds to click on\n',...
            'one of the response boxes.\n\n',...
            'If you miss any of the deadlines, you will see a message "Missed deadline!"\n',...
            'After every completed trial you will be given feedback on whether your response\n',...
            'was correct and how much time you took to complete the trial.\n\n',...
            'Press space to begin the Practice Round 2.'];

    round3Msg = ['Practice Round 3 involves actual time deadlines.\n\n',...
            'You will be given no feedback on your performance.\n\n',...
            'Press space to begin the Practice Round 3.'];
    
    [numTrials, ~] = size(practiceTrialMatrix);
    
    round1End = numTrials/4;
    round2End = numTrials/4 * 2;
    
    displayMsg(round1Msg, ptb);
    practiceRound1(practiceTrialMatrix(1:round1End,:), ptb, block);
    displayMsg(round2Msg, ptb);
    practiceRound2(practiceTrialMatrix(round1End+1:round2End,:), ptb, block);
    displayMsg(round3Msg, ptb);
    practiceRound3(practiceTrialMatrix(round2End+1:numTrials,:), ptb);
    
end